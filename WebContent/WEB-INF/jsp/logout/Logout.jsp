<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html dir="ltr" lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, user-scalable=yes, maximum-scale=1.0, minimum-scale=1.0">
<meta name="description" content="ヤスイ家具株式会社 | イケてる家具をお手頃な値段であなたに">
<meta name="keywords" content="">
<title>ログアウト | やすい家具ならヤスイ家具</title>
<link rel="stylesheet" href="css/style.css" type="text/css"
	media="screen">
<link rel="stylesheet" href="css/purchase.css" type="text/css"
	media="screen">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/script.js"></script>
<script>function goIndex(){location='<c:out value="${serverURL}"/>/index';}</script>
</head>
<body>
	<!-- ヘッダー -->
	<c:import url="/WEB-INF/jsp/common/header.jsp" />
	<div id="wrapper">
		<!-- コンテンツ -->
		<section id="main" class="content">
			<h1 class="heading">ログアウト</h1>
			<p>ログアウトしました。</p>
		</section>
		<div class="return-wrapper">
			<input class="return-button" type="submit" value="戻る"
				onClick="goIndex()" />
		</div>
	</div>
	<!-- / WRAPPER -->
	<!-- フッター -->
	<c:import url="/WEB-INF/jsp/common/footer.jsp" />
	<!-- / フッター -->
</body>
</html>