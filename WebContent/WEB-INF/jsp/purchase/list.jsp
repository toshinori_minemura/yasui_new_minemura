<!DOCTYPE html>
<html dir="ltr" lang="ja">
<head>
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/include.jsp" />
<link rel="stylesheet" href="css/purchase.css" type="text/css"
	media="screen">
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/header.jsp" />
	<!-- メイン画像 -->
	<div id="mainBanner" class="mainImg">
		<div class="inner">
			<img src="images/mainImg.jpg" alt="" width="930" height="290">
			<div class="sloganB">
				<h2>わたしたちの選んだ家具を使ってもらいたい</h2>
				<h3>わたしたちのお気に入りを、みなさんのお気に入りにしてもらうのがわたしたちの夢です</h3>
			</div>
		</div>
	</div>
	<!-- / メイン画像 -->
	<main id="wrapper">
		<c:if test="${not empty errormessage}">
			<c:forEach var="message" items="${errormessage}"
				varStatus="statusError">
				<span class="errormsg">(エラー)：${message}</span>
				<br />
			</c:forEach>
			<c:remove var="errormessage" />
			<%--表示が終わったエラーメッセージは削除する --%>
		</c:if>
		<form method="POST" action="PurchaseConfirm">
			<!-- 4カラム -->
			<section class="gridWrapper">
				<c:forEach var="item" items="${items}" varStatus="status">
					<article class="grid">
						<h3>
							<c:out value="${item.itemName}" />
						</h3>
						<p class="img">
							<img src="${fn:escapeXml(contextPath)}${fn:escapeXml(item.imageUrl)}" width="220"
								height="220" alt="${fn:escapeXml(item.itemName)}">
						</p>
						<dl>
							<dt>商品ID：</dt>
							<dd>
								<c:out value="${item.itemId}" />
							</dd>
							<dt>価格：</dt>
							<dd>
								<fmt:setLocale value="ja_JP" />
								<fmt:formatNumber value="${item.price}" type="currency" />
							</dd>
							<dt>サイズ：</dt>
							<dd>
								<c:out value="${item.itemSize}" />
							</dd>
							<!-- 注文数量の処理 -->
							<c:choose>
								<%--stockの数が0以下の場合 --%>
								<c:when test="${item.quantity <=0}">
									<dt>在庫：</dt>
									<dd>なし</dd>
									<dt>注文数：</dt>
									<dd>現在注文できません</dd>
								</c:when>
								<c:otherwise>
									<dt>在庫：</dt>
									<dd>
										<c:out value="${item.quantity}" />
									</dd>
									<dt>注文数：</dt>
									<dd>
										<input class="order-quantity" type="number"
											name="${fn:escapeXml(item.itemId)}" size="25" maxlength="8"
											min="0" max="${fn:escapeXml(item.quantity)}" style="text-align: right"
											value='<c:out value="${item.order}" default="0" />' />
									</dd>
								</c:otherwise>
							</c:choose>
						</dl>
						<div class="gridEnd"></div>
					</article>
				</c:forEach>
			</section>
			<!-- / 4カラム -->
			<div class="submit-wrapper">
				<input class="submit-button" type="submit" value="注文を確認する" />
			</div>
		</form>
	</main>
	<!-- / WRAPPER -->
	<c:import url="/WEB-INF/jsp/common/footer.jsp" />
	<c:remove var="items" />
</body>
</html>