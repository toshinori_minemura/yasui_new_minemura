<!DOCTYPE html>
<html dir="ltr" lang="ja">
<head>
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/include.jsp" />
<link rel="stylesheet" href="css/purchase.css" type="text/css"
	media="screen">
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/header.jsp" />
	<main id="wrapper">
		<!-- コンテンツ -->
		<section id="main">
			<section class="content">
				<h1 class="heading">注文の確認</h1>
				<article>
					<table class="itemlist">
						<thead>
							<tr>
								<th>商品番号</th>
								<th>商品名</th>
								<th>価格</th>
								<th>注文数</th>
								<th>小計</th>
							</tr>
						</thead>
						<tbody>
							<c:set var="sum" value='0' />
							<c:forEach var="orderitem" items="${items}" varStatus="status">
								<tr>
									<td data-label="商品番号"><c:out value="${orderitem.itemId}" />
									</td>
									<td data-label="商品名"><c:out value="${orderitem.itemName}" />
									</td>
									<td data-label="価格" class="right-aligned"><fmt:setLocale
											value="ja_JP" /> <fmt:formatNumber
											value="${orderitem.price}" type="currency" /></td>
									<td data-label="注文数" class="right-aligned"><c:out
											value="${orderitem.order}" /></td>
									<td data-label="小計" class="right-aligned"><fmt:formatNumber
											value="${orderitem.order * orderitem.price}" type="currency" />
									</td>
								</tr>
								<c:set var="sum"
									value="${sum+(orderitem.order * orderitem.price)}" />
							</c:forEach>
						</tbody>
					</table>
					<table class="sumlist">
						<thead>
							<tr>
								<th>合計</th>
								<th>消費税額</th>
								<th>支払総額</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="total" data-label="合計"><fmt:formatNumber
										value="${sum}" type="currency" /></td>
								<c:set var="tax" value="${sum*rate}" />
								<td class="tax" data-label="消費税額"><fmt:formatNumber
										value="${tax}" type="currency" /></td>
								<td class="grand-total" data-label="支払総額"><fmt:formatNumber
										value="${sum + tax}" type="currency" /></td>
							</tr>
						</tbody>
					</table>
				</article>
			</section>
		</section>
		<!-- / コンテンツ -->
		<div class="submit-wrapper">
			<form action="PurchaseComplete" method="post">
				<input class="submit-button" type="submit" value="注文を確定する" />
			</form>
		</div>
	</main>
	<div class="return-wrapper">
		<form action="ListItem" method="post">
			<input class="return-button" type="submit" value="戻る" />
		</form>
	</div>
	<!-- / WRAPPER -->
	<c:import url="/WEB-INF/jsp/common/footer.jsp" />
</body>
</html>
