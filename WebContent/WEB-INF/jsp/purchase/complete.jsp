<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html dir="ltr" lang="ja">
<head>
<meta charset="UTF-8">
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/include.jsp" />
<link rel="stylesheet" href="css/purchase.css" type="text/css"
	media="screen">
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/header.jsp" />
	<main id="wrapper">
		<section id="main" class="content">
			<h1 class="heading">購入完了</h1>
			<p>商品の購入が完了しました。<br>
			商品購入代金のお支払いは、商品同梱の振り込み用紙からお願い致します。<br>
			振り込みは主要コンビニエンスストアチェーンでお支払いいただけます。</p>
		</section>
		<div class="return-wrapper">
			<form action="ListItem" method="post">
				<input class="return-button" type="submit" value="戻る" />
			</form>
		</div>
	</main>
	<!-- / WRAPPER -->
	<c:import url="/WEB-INF/jsp/common/footer.jsp" />
</body>
</html>