<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/includeAdmin.jsp" />
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/headerAdmin.jsp" />
	<c:import url="/WEB-INF/jsp/common/navmenu.jsp" />
	<section id="main" class="content">
		<h1>下記の商品を削除しますか？</h1>
		<table class="itemlist">
			<tr>
				<th>商品番号</th>
				<th>商品名</th>
				<th>商品画像</th>
				<th>サイズ</th>
				<th>価格</th>
				<th>在庫</th>
			</tr>
			<c:forEach var="targetItem" items="${targetItems}" varStatus="status">
				<tr>
					<td><c:out value="${targetItem.itemId}" /></td>
					<td><c:out value="${targetItem.itemName}" /></td>
					<td><a href="${fn:escapeXml(contextPath)}${fn:escapeXml(item.imageUrl)}">商品画像</a></td>
					<td><c:out value="${targetItem.itemSize}" /></td>
					<fmt:setLocale value="ja_JP" />
					<td><fmt:formatNumber value="${targetItem.price}" type="currency" /></td>
					<td><fmt:formatNumber type="number" groupingUsed="true"
							value="${targetItem.quantity}" minFractionDigits="0" /></td>
				</tr>
			</c:forEach>
		</table>
		<div id="goreturn">
			<form method="POST" action="RemoveItem">
				<input class="submitButton" type="submit" name="goindex" value="戻る" />
			</form>
			<form method="POST" action="RemoveItemComplete">
				<input class="submitButton" type="submit" name="remove" value="削除する" />
			</form>
		</div>
	</section>
	<c:import url="/WEB-INF/jsp/common/footerAdmin.jsp" />
</body>
</html>