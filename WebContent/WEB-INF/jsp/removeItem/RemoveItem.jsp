<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/includeAdmin.jsp" />
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/headerAdmin.jsp" />
	<c:import url="/WEB-INF/jsp/common/navmenu.jsp" />
	<c:if test="${not empty errormessage}">
		<c:forEach var="message" items="${errormessage}">
			<span class="errormsg">(Error)：${message}</span>
			<br />
		</c:forEach>
		<c:remove var="errormessage" />
		<%--表示が終わったエラーメッセージは削除する --%>
	</c:if>
	<section id="main" class="content">
		<h1>商品一覧</h1>
		<p>削除したい商品のチェックボックスをチェックしてください</p>
		<form method="POST" action="RemoveItemConfirm">
			<table class="itemlist">
				<tr>
					<th>削除対象</th>
					<th>商品番号</th>
					<th>商品名</th>
					<th>商品画像</th>
					<th>サイズ</th>
					<th>価格</th>
					<th>在庫</th>
				</tr>
				<c:forEach var="item" items="${items}" varStatus="status">
					<tr>
						<td><input type="checkbox" name="${item.itemId}" value="on"></td>
						<td><c:out value="${item.itemId}" /></td>
						<td><c:out value="${item.itemName}" /></td>
						<td><a href="${fn:escapeXml(contextPath)}${fn:escapeXml(item.imageUrl)}">商品画像</a></td>
						<td><c:out value="${item.itemSize}" /></td>
						<fmt:setLocale value="ja_JP" />
						<td><fmt:formatNumber value="${item.price}" type="currency" /></td>
						<td><fmt:formatNumber type="number" groupingUsed="true"
								minFractionDigits="0" value="${item.quantity}" /></td>
					</tr>
				</c:forEach>
			</table>
			<div id="goreturn">
				<input class="submitButton" type="submit" name="submit" value="確認" />
				<input type="reset" name="reset" value="リセット" />
			</div>
		</form>
	</section>
	<c:import url="/WEB-INF/jsp/common/footerAdmin.jsp" />
</body>
</html>