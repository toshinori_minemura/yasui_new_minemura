<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!doctype html>
<html dir="ltr" lang="ja">
<head>
<title>安い家具のお求めならYASUI家具 | YASUI家具オンラインショップ</title>
<c:import url="/WEB-INF/jsp/common/include.jsp" />
<meta name="description" content="ヤスイ家具株式会社 | イケてる家具をお手頃な値段であなたに">
<meta name="keywords" content="">
<link rel="stylesheet" href="css/login.css" type="text/css"
	media="screen">
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/header.jsp" />
	<div id="wrapper">
		<!-- コンテンツ -->
		<section id="main">
			<section class="content">
				<h1 class="heading">ログイン</h1>
				<section>
					<c:if test="${not empty errormessage}">
						<ul style="list-style: none;">
						<c:forEach var="message" items="${errormessage}">
							<li class="errormsg">(Error)：${message}</li>
						</c:forEach>
						</ul>
						<c:remove var="errormessage" />
						<%--表示が終わったエラーメッセージは削除する --%>
					</c:if>
				</section>
				<section>
					<form method="POST" action="Login">
						<fieldset>
							<legend>ユーザー名：</legend>
							<input class="login-form" type="text" name="username" size="20"
								maxlength="30" value="${username}"
								autofocus placeholder="ユーザー名" />
						</fieldset>
						<fieldset>
							<legend>パスワード：</legend>
							<input class="login-form" type="password" name="password"
								size="20" maxlength="30" value=""
								placeholder="パスワード" />
						</fieldset>
						<fieldset>
							<input class="login-button" type="submit" name="submit"
								value="ログイン">
						</fieldset>
					</form>
				</section>
				<section>
					<p>※オンライン会員登録は現在準備中です。会員登録は店舗にてお願い致します。</p>
				</section>
			</section>
		</section>
	</div>

	<!-- / WRAPPER -->
	<c:import url="/WEB-INF/jsp/common/footer.jsp" />
</body>
</html>
