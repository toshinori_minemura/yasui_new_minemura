<!doctype html>
<html>
<head>
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/includeAdmin.jsp" />
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/headerAdmin.jsp" />
	<c:import url="/WEB-INF/jsp/common/navmenu.jsp" />
	<section id="main" class="content">
		<table class="itemlist">
			<tr>
				<th>商品番号</th>
				<th>商品名</th>
				<th>商品画像</th>
				<th>サイズ</th>
				<th>価格</th>
				<th>在庫</th>
			</tr>
			<tr>
				<td><c:out value="${newItem.itemId}" /></td>
				<td><c:out value="${newItem.itemName}" /></td>
				<td><a href="${fn:escapeXml(contextPath)}${fn:escapeXml(newItem.imageUrl)}">商品画像</a></td>
				<td><c:out value="${newItem.itemSize}" /></td>
				<fmt:setLocale value="ja_JP" />
				<td><fmt:formatNumber value="${newItem.price}" type="currency" /></td>
				<td><fmt:formatNumber type="number" groupingUsed="true"
						value="${newItem.quantity}" minFractionDigits="0" /></td>
			</tr>
		</table>
		<div id="goreturn">
			<form method="GET" action="AddItem">
				<input type="submit" name="goindex" value="戻る" />
			</form>
			<form method="POST" action="AddItemComplete">
				<input class="submitButton" type="submit" name="addItem"
					value="追加する" />
			</form>
		</div>
	</section>
	<c:import url="/WEB-INF/jsp/common/footerAdmin.jsp" />
</body>
</html>