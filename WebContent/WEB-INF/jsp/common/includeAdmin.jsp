<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" type="text/css" href="css/site.css" />
<link rel="stylesheet" type="text/css" href="css/validationEngine.jquery.css" />
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, maximum-scale=1.0, minimum-scale=1.0">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript" src="/YasuiMobile/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="/YasuiMobile/js/jquery.validationEngine-ja.js"></script>
<%--
  //デバッグ用httpヘッダ確認コード
  @SuppressWarnings("unchecked")
  java.util.Enumeration<String> headernames = request.getHeaderNames();
  while (headernames.hasMoreElements()) {
    String name = (String) headernames.nextElement();
    @SuppressWarnings("unchecked")
    java.util.Enumeration<String> headervals = request.getHeaders(name);
    while (headervals.hasMoreElements()) {
      String val = (String) headervals.nextElement();
      System.out.println("ヘッダ名：" + name + " 値：" + val);
    }
  }
--%>