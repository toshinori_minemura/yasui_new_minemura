<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html dir="ltr" lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=yes, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="ヤスイ家具株式会社 | イケてる家具をお手頃な値段であなたに">
    <meta name="keywords" content="">
    <title>認証失敗 | やすい家具ならヤスイ家具</title>
    <c:import url="/WEB-INF/jsp/common/include.jsp" />
    <link rel="stylesheet" href="css/purchase.css" type="text/css" media="screen">
    <style>
        .content{
            height:350px;
        }
        .errorHeader{
            font-size: 1.2em;
            font-style: oblique;
            font-weight: bold;
        }
        .errorReason{
            list-style-type: square;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
    <SCRIPT>
<!--
	function autoLink() {
		location.href = "index";
	}
	setTimeout("autoLink()", 10000);
// -->
</SCRIPT>
</head>

<body>
    <!-- ヘッダー -->
    <header id="header">
        <div class="inner">
            <!-- ロゴ -->
            <div class="logo">
                <a href="index.html">Yasui Furniture</a>
                <div class="boxContainer">
                    <div class="box">
                        <h1>認証に失敗しました</h1></div>
                </div>
            </div>
            <!-- / ロゴ -->

            <c:import url="/WEB-INF/jsp/common/nav.jsp"></c:import>
        </div>
    </header>
    <!-- / ヘッダー -->

    <div id="wrapper">

        <!-- コンテンツ -->
        <section id="main">
            <section class="content">
                <h3 class="heading">エラーが発生しました</h3>
                <article>
                    <div class="center-image"><img alt="sorry" src="images/sad.gif" width="150" height="150"></div>
                    <p>現在の画面において、致命的なエラーが発生しました。しばらくたってもエラーが改善されない場合には、本画面のキャプチャを添付の上、 <a href="mailto:foo@bar.com">ヤスイ家具ヘルプデスク </a> までご連絡ください。</p>
                    <h4 class="errorHeader">エラーの理由</h4>
                    <ul class="errorReason">
                        <li>${requestScope['javax.servlet.error.message']}</li>
                    </ul>
                </article>
            </section>
        </section>
        <div class="return-wrapper">
            <form action="index" method="post">
                <input class="return-button" type="submit" value="ログイン" />
            </form>
        </div>
        	<c:if test="${initParam['mode.debug']=='true'}">
		<table id="table01">
			<tr>
				<th>項目</th>
				<th>メッセージ</th>
			</tr>
			<tr>
				<th valign="top">例外情報（概要）</th>
				<td>${requestScope['javax.servlet.error.exception']}<br /></td>
			</tr>
			<tr>
				<th valign="top">例外クラス</th>
				<td>${requestScope['javax.servlet.error.exception_type']}<br /></td>
			</tr>
			<tr>
				<th valign="top">例外が発生したファイル</th>
				<td>${requestScope['javax.servlet.error.request_uri']}<br /></td>
			</tr>
			<tr>
				<th valign="top">ステータスコード</th>
				<td>${requestScope['javax.servlet.error.status_code']}<br /></td>
			</tr>
		</table>
	</c:if>
    </div>
    <!-- / WRAPPER -->
	<c:import url="/WEB-INF/jsp/common/footer.jsp" />

</body>

</html>
