<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html dir="ltr" lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=yes, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="ヤスイ家具株式会社 | イケてる家具をお手頃な値段であなたに">
    <meta name="keywords" content="">
    <title>認証失敗 | やすい家具ならヤスイ家具</title>
    <c:import url="/WEB-INF/jsp/common/include.jsp" />
    <link rel="stylesheet" href="css/purchase.css" type="text/css" media="screen">
    <style>
        .content{
            height:350px;
        }
        .errorHeader{
            font-size: 1.2em;
            font-style: oblique;
            font-weight: bold;
        }
        .errorReason{
            list-style-type: square;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
    <SCRIPT>
<!--
	function autoLink() {
		location.href = "index";
	}
	setTimeout("autoLink()", 10000);
// -->
</SCRIPT>
</head>

<body>
    <!-- ヘッダー -->
    <header id="header">
        <div class="inner">
            <!-- ロゴ -->
            <div class="logo">
                <a href="index.html">Yasui Furniture</a>
                <div class="boxContainer">
                    <div class="box">
                        <h1>お探しの画面が見つかりませんでした</h1></div>
                </div>
            </div>
            <!-- / ロゴ -->

            <c:import url="/WEB-INF/jsp/common/nav.jsp"></c:import>
        </div>
    </header>
    <!-- / ヘッダー -->

    <div id="wrapper">

        <!-- コンテンツ -->
        <section id="main">
            <section class="content">
                <h1 class="heading">お探しの画面が見つかりませんでした。</h1>
                <article>
                    <p>URLの指定が間違っているか、不正です。</p>
                    <h2 class="errorHeader">失敗の理由</h2>
                    <ul class="errorReason">
                        <li>戻るボタンなどの利用はなさらないでください。</li>
                    </ul>
                </article>
            </section>
        </section>
        <div class="return-wrapper">
            <form action="index" method="post">
                <input class="return-button" type="submit" value="ログイン" />
            </form>
        </div>
    </div>
    <!-- / WRAPPER -->
	<c:import url="/WEB-INF/jsp/common/footer.jsp" />

</body>

</html>
