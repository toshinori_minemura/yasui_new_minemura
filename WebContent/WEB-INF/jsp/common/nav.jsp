<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
            <!-- メインナビゲーション -->
            <nav id="mainNav">
                <a class="menu" id="menu"><span>MENU</span></a>
                <div class="panel">
                    <ul>
                    	<c:if test="${not empty sessionScope['username']}">
                    	<li id="list" class="active"><a href="ListItem">商品購入<br><span class="panel-caption">Purchase</span></a></li>
                    	</c:if>
                        <li id="greeting"><a href="Greeting">ご挨拶<br><span class="panel-caption">Greeting</span></a></li>
                        <li id="service"><a href="Service">サービス概要<br><span class="panel-caption">Service</span></a></li>
                        <li id="approach"><a href="Approach">弊社の取り組み<br><span class="panel-caption">Approach</span></a></li>
                        <li id="company"><a href="Company">会社情報<br><span class="panel-caption">Company</span></a></li>
                        <li id="contact"><a href="Contact">問い合わせ<br><span class="panel-caption">Contact</span></a></li>
                        <c:choose>
                        <c:when test="${empty sessionScope['username']}">
                        <li id="index" class="active"><a href="index">ログイン<br><span class="panel-caption">Login</span></a></li>
                        </c:when>
                        <c:otherwise>
                        <li id="logout"><a href="Logout">ログアウト<br><span class="panel-caption">Logout</span></a></li>
                        </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </nav>
            <!-- メインナビゲーション -->