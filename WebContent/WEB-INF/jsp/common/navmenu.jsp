<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<div id=nav>
	<ul>
		<c:if test="${not empty sessionScope['username']}">
			<li>顧客ID［${fn:escapeXml(sessionScope['username'])}
				（${fn:escapeXml(sessionScope['displayName'])}）］</li>
		</c:if>
		<c:if test="${sessionScope['role'] == 'administrator'}">
			<li><a href="AddItem">商品追加</a></li>
			<li><a href="ChangeStock">在庫数量変更</a></li>
			<li><a href="RemoveItem">商品削除</a></li>
		</c:if>
			<li><a href="ListItem">商品の選択（クライアント系画面）</a></li>
			<li><a href="Logout">ログアウト</a></li>
	</ul>
</div>
<%-- ログイン中の画面はグローバルナビゲーションの下にhrを入れる --%>
<c:if test="${not empty sessionScope['username']}">
	<hr>
</c:if>