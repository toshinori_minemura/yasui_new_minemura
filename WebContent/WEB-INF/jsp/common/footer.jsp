<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
        <!-- フッター -->
        <footer id="footer">
            <div class="inner">

                <!-- 3カラム -->
                <section class="gridWrapper">

                    <article class="grid">
                        <!-- ロゴ -->
                        <p class="logo"><a href="index">Yasui Furniture<br><span>イケてる家具をお手頃な値段であなたに</span></a></p>
                        <!-- / ロゴ -->
                    </article>

                    <article class="grid">
                    	<address>
                        <!-- 電話番号+受付時間 -->
                        <p class="tel">電話: <strong>012-3456-7890</strong></p>
                        <p>受付時間: 平日 AM 10:00 〜 PM 9:00</p>
                        <!-- / 電話番号+受付時間 -->
                        </address>
                    </article>

                    <article class="grid copyright">
                        Copyright(c) 2011-2020 Yasui Furniture Inc. All Rights Reserved.
                    </article>

                </section>
                <!-- / 3カラム -->

            </div>
        </footer>
        <!-- / フッター -->
