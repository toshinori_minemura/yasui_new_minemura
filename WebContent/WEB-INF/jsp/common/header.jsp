<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
    <!-- ヘッダー -->
    <header id="header">
        <div class="inner">
            <!-- ロゴ -->
            <div class="logo">
                <a href="index">Yasui Furniture</a>
                <div class="boxContainer">
                    <div class="box">
                        <h1><c:out value="${title}" /></h1>
                    </div>

                    <div class="box"><c:if test="${not empty sessionScope['username']}"><c:out value="${username}" />（<c:out value="${displayName}" />）</c:if></div>
                </div>
                <h1></h1>
            </div>
            <!-- / ロゴ -->

		<c:import url="/WEB-INF/jsp/common/nav.jsp"></c:import>
        </div>
        </header>
        <!-- / ヘッダー -->