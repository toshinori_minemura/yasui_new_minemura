<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/includeAdmin.jsp" />
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/headerAdmin.jsp" />
	<c:import url="/WEB-INF/jsp/common/navmenu.jsp" />
	<c:if test="${not empty errormessage}">
		<section>
			<ul style="list-style: none;">
				<c:forEach var="message" items="${errormessage}">
					<li class="errormsg">(Error)：${message}</li>
				</c:forEach>
			</ul>
			<c:remove var="errormessage" />
			<%--表示が終わったエラーメッセージは削除する --%>
		</section>
	</c:if>
	<section id="main">
		<h2>変更したい在庫数量を指定してください</h2>
		<form method="POST" action="ChangeStockConfirm">
			<table class="itemlist">
				<tr bgcolor="#00ccff">
					<th>商品番号</th>
					<th>商品名</th>
					<th>商品画像</th>
					<th>サイズ</th>
					<th>価格</th>
					<th>現在の在庫</th>
					<th>変更後の在庫</th>
				</tr>
				<c:forEach var="item" items="${changeStock}" varStatus="status">
					<c:choose>
						<c:when test="${status.count%2==0}">
							<c:set var="bgcol" value="#FFffCC" />
						</c:when>
						<c:otherwise>
							<c:set var="bgcol" value="#EEeeAA" />
						</c:otherwise>
					</c:choose>
					<tr bgcolor="${bgcol}">
						<td><c:out value="${item.itemId}" /></td>
						<td><c:out value="${item.itemName}" /></td>
						<td><a href="${fn:escapeXml(contextPath)}${fn:escapeXml(item.imageUrl)}">商品画像</a></td>
						<td><c:out value="${item.itemSize}" /></td>
						<fmt:setLocale value="ja_JP" />
						<td><fmt:formatNumber value="${item.price}" type="currency" /></td>
						<td><fmt:formatNumber type="number" groupingUsed="true"
								minFractionDigits="0" value="${item.quantity}" /></td>
						<td><input type="number" name="${fn:escapeXml(item.itemId)}"
							size="25" maxlength="8" min="0" max="99999999"
							style="text-align: right" value="${fn:escapeXml(item.quantity)}" /></td>
					</tr>
				</c:forEach>
			</table>
			<div id="goreturn">
				<input class="submitButton" type="submit" name="submit" value="確認" />
				<input type="reset" name="reset" value="リセット" />
			</div>
		</form>
	</section>
	<c:import url="/WEB-INF/jsp/common/footerAdmin.jsp" />
</body>
</html>