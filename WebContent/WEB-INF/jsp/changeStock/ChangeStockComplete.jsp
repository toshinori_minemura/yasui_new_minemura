<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/includeAdmin.jsp" />
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/headerAdmin.jsp" />
	<c:import url="/WEB-INF/jsp/common/navmenu.jsp" />
	<section id="main">
		<h1>在庫数の変更を完了しました</h1>
	</section>
	<div id="goreturn">
		<form method="POST" action="ChangeStock">
			<input class="LButton" type="submit" name="return" value="在庫変更に戻る" />
		</form>
		<form method="POST" action="ListItem">
			<input class="LLButton" type="submit" name="return"
				value="商品選択（クライアント系)に戻る" />
		</form>
	</div>
	<c:import url="/WEB-INF/jsp/common/footerAdmin.jsp" />
</body>
</html>