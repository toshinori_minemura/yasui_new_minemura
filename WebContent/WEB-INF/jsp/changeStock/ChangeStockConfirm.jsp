<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<c:import url="/WEB-INF/jsp/common/tdk.jsp" />
<c:import url="/WEB-INF/jsp/common/includeAdmin.jsp" />
</head>
<body>
	<c:import url="/WEB-INF/jsp/common/headerAdmin.jsp" />
	<c:import url="/WEB-INF/jsp/common/navmenu.jsp" />
	<section id="main">
		<h2>下記の内容で在庫を変更しますか？</h2>
		<table class="itemlist" class="content">
			<tr>
				<th>商品番号</th>
				<th>商品名</th>
				<th>商品画像</th>
				<th>サイズ</th>
				<th>価格</th>
				<th>新在庫</th>
			</tr>
			<c:forEach var="item" items="${changeStock}" varStatus="status">
				<tr>
					<td><c:out value="${item.itemId}" /></td>
					<td><c:out value="${item.itemName}" /></td>
					<td><a href="${fn:escapeXml(contextPath)}${fn:escapeXml(item.imageUrl)}">商品画像</a></td>
					<td><c:out value="${item.itemSize}" /></td>
					<fmt:setLocale value="ja_JP" />
					<td><fmt:formatNumber value="${item.price}" type="currency"
							groupingUsed="true" minFractionDigits="0" /></td>
					<td><fmt:formatNumber type="number" groupingUsed="true"
							value="${item.quantity}" minFractionDigits="0" /></td>
				</tr>
			</c:forEach>
		</table>
		<div id="goreturn">
			<form method="GET" action="ChangeStock">
				<input class="submitButton" type="submit" name="goindex" value="戻る" />
			</form>
			<form method="POST" action="ChangeStockComplete">
				<input class="submitButton" type="submit" name="change" value="変更する" />
			</form>
		</div>
	</section>
	<c:import url="/WEB-INF/jsp/common/footerAdmin.jsp" />
</body>
</html>