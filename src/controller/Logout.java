package controller;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet(urlPatterns = "/Logout")
public class Logout extends HttpServlet {

	private static final long serialVersionUID = -2936421374161417176L;

	protected void doGet(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		//ログアウト処理が成功に終わった時の転送先
		String destination = "/WEB-INF/jsp/logout/Logout.jsp";
		HttpSession session = request.getSession(false);
		String username = (String) session.getAttribute("username");

		//サーブレットコンテキストのログに出力
		ServletContext context = request.getServletContext();
		StringBuilder msg = new StringBuilder();
		msg.append(LocalDateTime.now());
		msg.append(",logout:success,");
		msg.append("user name:"+username+",");
		msg.append(request.getHeader("user-agent"));
		context.log(msg.toString());

		if(session!=null){
			request.setAttribute("serverURL", session.getAttribute("serverURL"));
			session.invalidate();
		}

		request.getRequestDispatcher(destination).forward(request, response);
	}
}
