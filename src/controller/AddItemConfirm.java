package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.AddItemLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/AddItemConfirm")
public class AddItemConfirm extends HttpServlet {
	private static final long serialVersionUID = 8077157819137881414L;

	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {

		//デフォルトの転送先;
		String destination = "/WEB-INF/jsp/addItem/AddItemConfirm.jsp";
		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();
		//セッションの取得
		HttpSession session = request.getSession(false);

		//リクエストからパラメーターを取り出す
		Map<String,String[]> params = request.getParameterMap();

		//モデルクラスでパラメーターのバリデーション（検証）を行う
		AddItemLogic addItemConfirmLogic = new AddItemLogic();
		ItemVO newItem=null;
		try {
			newItem = addItemConfirmLogic.itemCheck(params);
		} catch ( SQLException | NamingException | IllegalArgumentException e) {
			error.add("追加する商品情報が適切ではありません");
		}

		//VOが空だったら（エラーあり）
		if(newItem==null||!error.isEmpty()){
			//ロジッククラスで発生したエラーを全部追加する
			error.addAll(addItemConfirmLogic.getErrors());
			destination="/AddItem";
			//完成したエラーメッセージ用ArrayListをrequestに格納
			request.setAttribute("errormessage",error);
		}else{
			//完了のサーブレットまで渡したいのでsessionに格納する
			session.setAttribute("newItem",newItem);
		}

		//目的地に転送
		request.getRequestDispatcher(destination).forward(request, response);

	}
}
