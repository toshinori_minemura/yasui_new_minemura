package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ListItemLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/ChangeStock")
public class ChangeStock extends HttpServlet {
	private static final long serialVersionUID = -7228890911479890546L;

	protected void doGet(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {

		String destination= "/WEB-INF/jsp/changeStock/ChangeStock.jsp";
		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();

		//confirmなどから戻された場合のエラーをマージする
		@SuppressWarnings("unchecked")
		ArrayList<String> errorSent = (ArrayList<String>) request.getAttribute("error");
		if(errorSent!=null&&!errorSent.isEmpty()) {
			error.addAll(errorSent);
		}

		//セッションの取得（なければ作成）
		HttpSession session = request.getSession(false);

		//商品一覧のArrayList作成
		ArrayList<ItemVO> itemList = null;
		//ロジッククラスのインスタンス作成
		ListItemLogic listItemLogic = new ListItemLogic();

		//戻るボタンを押された場合のオーダーを含むItem情報Listを取得
		@SuppressWarnings("unchecked")
		ArrayList<ItemVO> backwardList = (ArrayList<ItemVO>)session.getAttribute("items");
		session.removeAttribute("items");

		//セッションからitemsを取得できた場合は戻るボタンを押されている
		try{
			if(backwardList!=null){
				itemList=listItemLogic.getItemList(backwardList);
			}else{
				itemList=listItemLogic.getItemList();
			}
		}catch(SQLException|NamingException|IOException|IllegalArgumentException e){
			error.add("在庫の変更を行う商品の一覧の取得に失敗しました："+e.getMessage());
		}
		if(!error.isEmpty()) {
			//完成したエラーメッセージ用ArrayListをrequestに格納
			request.setAttribute("errormessage",error);
		}
		//変更対象の商品のArrayListをリクエストに格納
		request.setAttribute("changeStock", itemList);

		//RequestDispatcherオブジェクトを取得して転送
		request.getRequestDispatcher(destination).forward(request, response);

	}
	//Confirmからの戻りはPOSTメソッド
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {
		this.doGet(request, response);
	}

}
