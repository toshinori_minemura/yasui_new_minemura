package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ListItemLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/PurchaseConfirm")
public class PurchaseConfirm extends HttpServlet {
	private static final long serialVersionUID = 7612802445832537582L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final double TAX_RATE = 0.1;

		//デフォルトの遷移先
		String destination = "/WEB-INF/jsp/purchase/confirm.jsp";

		// セッションの取得（なければ作成）
		HttpSession session = request.getSession(false);

		// 注文用ArrayListの作成
		ArrayList<ItemVO> orderedItems = new ArrayList<ItemVO>();

		//エラーメッセージ処理List
		ArrayList<String> error = new ArrayList<String>();

		//ロジッククラスをインスタンス化
		ListItemLogic listItemLogic = new ListItemLogic();
		//リクエストよりパラメーターのMapを取得
		Map<String,String[]> orderItemMap = request.getParameterMap();

		ArrayList<ItemVO> items=null;
		try {
			//リクエストのパラメーターから、注文情報を取得して、注文を含むItemVOのリストを作成する
			listItemLogic.confirm(
					orderItemMap,	//getParameterMapで取得したMap
					orderedItems);	//注文のあった商品だけのList
		} catch (NamingException|SQLException|IllegalArgumentException e) {
			error.add(e.getMessage()+":商品情報の取得で不具合が発生しています");
		}
		//送信されたMapの情報に不具合があったらエラーに追加
		if(listItemLogic.hasErrors()) {
			error.addAll(listItemLogic.getErrors());
		}
		//何らかのエラーがあったらエラーメッセージをsetして差し戻し
		if(!error.isEmpty()){
			session.setAttribute("errormessage",error);
			destination = "/ListItem";
			//もとのページに戻すので、単なる商品一覧のほうを戻す
			request.setAttribute("items", items);
		}else{
			//完成した注文用ArrayListをセッションに格納（上書き）
			session.setAttribute("items", orderedItems);
			session.setAttribute("rate", TAX_RATE);
		}

		//目的地に転送
		request.getRequestDispatcher(destination).forward(request, response);
		return;
	}


}
