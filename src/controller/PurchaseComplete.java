package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ProcessOrderLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/PurchaseComplete")
public class PurchaseComplete extends HttpServlet {

	private static final long serialVersionUID = -6036220739670048826L;

	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {

		String destination = "/WEB-INF/jsp/purchase/complete.jsp";
		//セッションの取得
		HttpSession session = request.getSession(false);
		//エラーメッセージ処理List
		ArrayList<String> error = new ArrayList<String>();

		@SuppressWarnings("unchecked")
		ArrayList<ItemVO> orderitems = (ArrayList<ItemVO>)session.getAttribute("items");
		if(orderitems==null) {
			error.add("注文情報が失われました。（例：30分以上無操作）");
		}else {
			ProcessOrderLogic logic = new ProcessOrderLogic();
			//処理の結果を受け取る変数
			int result=0;
			try{
				result = logic.processOrder(orderitems, (String)session.getAttribute("username"));
			}catch(SQLException|NamingException|IllegalArgumentException e){
				error.add("エラー："+e.getMessage()+"エラーコード："+result);
				//ロジックのエラー吸い出し
				error.addAll(logic.getErrors());
			}
		}
		session.removeAttribute("orderitems");
		session.removeAttribute("items");

		//エラーがある場合は商品情報一覧画面に差し戻し
		if(!error.isEmpty()){
			//エラーをrequestに格納
			session.setAttribute("errormessage",error);
			destination = "/ListItem";
		}

		//目的地に転送
		request.getRequestDispatcher(destination).forward(request, response);
	}
}
