package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AddItemLogic;
@WebServlet(urlPatterns = "/AddItem")
public class AddItem extends HttpServlet {
	private static final long serialVersionUID = -4971372719367601902L;

	protected void doGet(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {
		String destination=null;
		String nextId=null;
		//デフォルトの転送先
		destination = "/WEB-INF/jsp/addItem/AddItem.jsp";
		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();

		//confirmなどから戻された場合のエラーをマージする
		@SuppressWarnings("unchecked")
		ArrayList<String> errorSent = (ArrayList<String>) request.getAttribute("error");
		if(errorSent!=null&&!errorSent.isEmpty()) {
			error.addAll(errorSent);
		}

		AddItemLogic logic = new AddItemLogic();
		//追加するIDを準備する
		try{
			nextId = logic.getNextItemId();
		} catch (NamingException|SQLException|IllegalArgumentException e) {
			error.add(e.getMessage()+":新規商品IDの取得で不具合が発生しています");
		}
		//商品IDをRequestに格納
		request.setAttribute("nextId", nextId);
		if(!error.isEmpty()) {
			//完成したエラーメッセージ用ArrayListをrequestに格納
			request.setAttribute("errormessage",error);
		}
		//RequestDispatcherオブジェクトを取得、forwardメソッドで、処理をreceive.jspに転送
		request.getRequestDispatcher(destination).forward(request, response);
	}
	//Confirmからの戻りはPOSTメソッド
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {
		this.doGet(request, response);
	}

}
