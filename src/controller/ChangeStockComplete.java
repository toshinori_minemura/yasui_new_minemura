package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ChangeStockLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/ChangeStockComplete")
public class ChangeStockComplete extends HttpServlet {
	private static final long serialVersionUID = 2581432951251555792L;

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {

		//デフォルトの転送先
		String destination = "/WEB-INF/jsp/changeStock/ChangeStockComplete.jsp";
		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();
		//セッションの取得（なければ作成）
		HttpSession session = request.getSession(false);
		//在庫更新のロジック
		ChangeStockLogic logic = new ChangeStockLogic();

		int result=0;
		//セッションからリストを取り出す
		ArrayList<ItemVO> updatedItemList = (ArrayList<ItemVO>)session.getAttribute("changeStock");

		try{
			result=logic.updateStock(updatedItemList);
			if(result < 0){
				error.add("在庫が更新できませんでした。updateの処理が正常に終了していません。");
			}
		}catch(SQLException | NamingException | IllegalArgumentException e){
			error.add("在庫の更新ができませんでした。データベースの処理を"
					+ "失敗している可能性があります。"+e.getMessage());
		}

		if(!error.isEmpty()){
			destination="/ChangeStock";
			//完成したエラーメッセージ用ArrayListをrequestに格納
			request.setAttribute("errormessage",error);
		}
		session.removeAttribute("changeStock");

		//RequestDispatcherオブジェクトを取得して転送
		request.getRequestDispatcher(destination).forward(request, response);
	}
}
