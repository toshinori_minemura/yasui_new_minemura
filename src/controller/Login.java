package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Logger.CustomLog;
import model.AuthenticateLoginLogic;
import model.ListContentsLogic;
import secure.Digest;
import vo.ContentsVO;
import vo.UserVO;
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = -856700274893456786L;

	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {

		//エラー用のArrayList
		ArrayList<String> error = new ArrayList<String>();

		//セッションの取得
		HttpSession session = request.getSession(true);
		//エラーを引き継ぐ場合にArrayListにマージ処理
		@SuppressWarnings("unchecked")
		ArrayList<String> temp = (ArrayList<String>)session.getAttribute("errormessage");
		if(temp != null){
			for(String message:temp){
				error.add(message);
			}
			session.removeAttribute("errormessage");
		}
		//ログイン処理が成功に終わった時の転送先
		String destination = "/ListItem";

		HashMap<String,ContentsVO> contentsMap = null;
		StringBuffer serverURL = request.getRequestURL();
		String servletPath = request.getServletPath();

		//サーバーURLのみを取り出す
		serverURL.delete((serverURL.length()-servletPath.length()), serverURL.length());
		System.out.println(serverURL.toString());
		String contextPath = request.getContextPath();




		//顧客IDの取得
		String username=request.getParameter("username").trim();
		//パスワードの取得
		String password=request.getParameter("password").trim();

		//ハッシュ化
		Digest digest = new Digest(Digest.SHA512);
		String hashedPassword = digest.hex(password);
		//String hashedUserName = digest.hex(username); ハッシュ

		System.out.println(password);
		System.out.println(hashedPassword);




		//使用するオブジェクトの宣言
		UserVO userVO= null;
		//ユーザー名とパスワードが空でなかったらログインチェック
		if((username!=null&&!username.isBlank())&&(hashedPassword!=null&&!hashedPassword.isBlank())){
			try{
				AuthenticateLoginLogic loginCheck = new AuthenticateLoginLogic();
				//データベースに接続して、ユーザー情報を取得
				userVO = loginCheck.authCheck(username,hashedPassword) ;
				//ユーザーが存在した




				if(userVO!=null){
					//ログインログを作成 javaのturn10引用
					CustomLog customLog = CustomLog.getInstance();
					try {
						customLog.writeLog(username+"ログイン成功");
						}catch(IOException e) {
						}
					//ログイン属性詰め直し
					session.setAttribute("username", userVO.getName());
					session.setAttribute("displayName", userVO.getDisplayName());
					session.setAttribute("id", userVO.getId());
					session.setAttribute("role", userVO.getRole());
					if(userVO.getRole().equalsIgnoreCase("administrator")){
						destination="/AddItem";
					}else{
						destination="/ListItem";
					}
				}else{
					CustomLog customLog = CustomLog.getInstance();
					try {
						customLog.writeLog(username+"ログインエラー");
						}catch(IOException e) {
						}
					error.addAll(loginCheck.getErrors());

					//ユーザー名かパスワードが間違っている場合の処理
					error.add("ログイン処理に失敗しました。ユーザー名とパスワードが間違っている可能性があります");
				}
			}catch(SQLException|NamingException|IllegalArgumentException e){
				error.add("ログイン処理に失敗しました。データベースに障害が発生している可能性があります"+e.getMessage());
			}
		}else{//ユーザー名とパスワードが空だった場合の処理
			error.add("ログイン処理に失敗しました。ユーザー名とパスワードは省略できません。");
		}

		try{
			//データベースに接続して、TDK情報を管理する情報を取得
			ListContentsLogic listContents = new ListContentsLogic();
			contentsMap = listContents.getContentsMap();
		}catch(SQLException|NamingException e){
			error.add("コンテンツの取得処理に失敗しました"+e.getMessage());
			destination="/index";
		}

		//コンテンツ情報をセッションに設定
		session.setAttribute("contents", contentsMap);
		//サーバー名などをセッションに保存
		session.setAttribute("contextPath", contextPath);
		session.setAttribute("serverURL", serverURL);

		//サーブレットコンテキストのログに出力
		ServletContext context = request.getServletContext();
		StringBuilder msg = new StringBuilder();
		msg.append(LocalDateTime.now());
		if(!error.isEmpty()){
			msg.append(",login:success,");
		}else{
			msg.append(",login:fail,");
		}
		msg.append("user name:"+username+",");
		msg.append(request.getHeader("user-agent"));
		context.log(msg.toString());

		if(!error.isEmpty()){//異常系
			destination="/WEB-INF/jsp/common/LoginError.jsp";
			session.setAttribute("errormessage", error);
			//catalina.outにエラーを記録
			for(String message:error) {
				System.out.println("エラー："+message);
			}
			//異常系はエラーページにフォワード
			request.getRequestDispatcher(destination).forward(request, response);
			return;
		}

		response.sendRedirect(request.getContextPath()+destination);
		return;
	}
}

