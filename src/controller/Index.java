package controller;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet(urlPatterns = "/index")
public class Index extends HttpServlet{

	private static final long serialVersionUID = -8744723537486968453L;
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(true);
		ArrayList<String> errormessage = null;

		//エラーをリクエストに詰め替え
		if(session != null){
			errormessage = (ArrayList<String>)session.getAttribute("errormessage");
			if(errormessage!=null&&!errormessage.isEmpty()) {
				session.removeAttribute("errormessage");
				request.setAttribute("errormessage", errormessage);
			}
		}

		//ログアウト処理が成功に終わった時の転送先
		String destination = "/WEB-INF/jsp/index.jsp";
		//目的地に転送
		request.getRequestDispatcher(destination).forward(request, response);
	}
}