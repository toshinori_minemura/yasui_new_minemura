package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.RemoveItemLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/RemoveItemComplete")
public class RemoveItemComplete extends HttpServlet {
	private static final long serialVersionUID = 1059705617163017431L;

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {

		String destination= "/WEB-INF/jsp/removeItem/RemoveItemComplete.jsp";
		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();
		//セッションの取得
		HttpSession session = request.getSession(false);
		ArrayList<ItemVO> targetItems = null;
		if(session!=null) {
			targetItems = (ArrayList<ItemVO>)session.getAttribute("targetItems");
		}
		//ロジックのインスタンス生成
		RemoveItemLogic logic = new RemoveItemLogic();

		if ( targetItems != null ){
			try{
				logic.removeItem(targetItems);
			}catch(SQLException|NamingException|IllegalArgumentException e){
				error.add("削除処理が失敗しました。DAOの呼び出しに失敗している可能性があります。");
			}
		}else{
			error.add("削除対象の商品リストを取得できませんでした。");
		}
		session.removeAttribute("targetItems");
		if(!error.isEmpty()){
			//エラーがあったので削除の初期画面にエラーメッセージつきで差し戻し
			destination="/RemoveItem";
			//完成したエラーメッセージ用ArrayListをrequestに格納
			request.setAttribute("errormessage",error);
		}

		//RequestDispatcherオブジェクトを取得
		RequestDispatcher rd = request.getRequestDispatcher(destination);
		//forwardメソッドで、処理をreceive.jspに転送
		rd.forward(request, response);
	}


}
