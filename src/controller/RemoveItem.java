package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ListItemLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/RemoveItem")
public class RemoveItem extends HttpServlet {
	private static final long serialVersionUID = -4751992084637285363L;

	protected void doGet(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {
		String destination="/WEB-INF/jsp/removeItem/RemoveItem.jsp";

		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();

		//confirmなどから戻された場合のエラーをマージする
		@SuppressWarnings("unchecked")
		ArrayList<String> errorSent = (ArrayList<String>) request.getAttribute("error");
		if(errorSent!=null&&!errorSent.isEmpty()) {
			error.addAll(errorSent);
		}

		//セッションの取得
		HttpSession session = request.getSession(false);
		if(session!=null) {
			session.removeAttribute("items");
		}
		//商品一覧のArrayList作成
		ArrayList<ItemVO> itemList = new ArrayList<ItemVO>();
		//ロジッククラスのインスタンス作成
		ListItemLogic listItemLogic = new ListItemLogic();
		//商品情報一覧の取得
		try{
			itemList=listItemLogic.getItemList();
		}catch(SQLException|NamingException|IOException|IllegalArgumentException e){
			e.printStackTrace();
			error.add("削除対象の商品情報一覧を取得できませんでした:"+e.getMessage());
		}
		if(!error.isEmpty()){
			//完成したエラーメッセージ用ArrayListをRequestに格納
			request.setAttribute("errormessage",error);
			//トップ画面に転送
			destination="/index";
		}

		//変更対象の商品のArrayListをrequestに格納
		request.setAttribute("items", itemList);

		//RequestDispatcherオブジェクトを取得して転送
		request.getRequestDispatcher(destination).forward(request, response);

	}
	//Confirmからの戻りはPOSTメソッド
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {
		this.doGet(request, response);
	}
}
