package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.RemoveItemLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/RemoveItemConfirm")
public class RemoveItemConfirm extends HttpServlet {
	private static final long serialVersionUID = 3842549697179632660L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String destination="/WEB-INF/jsp/removeItem/RemoveItemConfirm.jsp";

		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();
		//削除商品用ArrayListの作成
		ArrayList<ItemVO> targetItems = null;
		// セッションの取得
		HttpSession session = request.getSession(false);

		//リクエストよりパラメーターのMapを取得
		Map<String,String[]> itemMap = request.getParameterMap();
		//ロジッククラスのインスタンス作成
		RemoveItemLogic removeItemLogic = new RemoveItemLogic();
		//最新の商品一覧（在庫付き）を取得
		try{
			//ロジッククラスの呼び出し
			targetItems=removeItemLogic.checkRemoveRequest(itemMap);

			// 注文が0行だったらエラー
			if (targetItems.size() == 0){
				error.add("有効な削除指定が0のため、削除できません");
			}
		} catch (NamingException|SQLException|IllegalArgumentException e) {
			error.add(e.getMessage()+":商品情報の取得で不具合が発生しています");
		}

		//エラーの有無で正常系と異常系を分ける
		if(!error.isEmpty()){//異常系
			//完成したエラーメッセージ用ArrayListをセッションに格納
			request.setAttribute("errormessage",error);
			//エラーがあったので削除の初期画面に差し戻し
			destination = "/RemoveItem";
		}else{//正常系
			//確認済削除対象商品情報ArrayListをセッションに格納
			session.setAttribute("targetItems", targetItems);

			//RequestDispatcherオブジェクトを取得して転送
			request.getRequestDispatcher(destination).forward(request, response);
		}
	}
}
