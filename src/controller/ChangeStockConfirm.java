package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ChangeStockLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/ChangeStockConfirm")
public class ChangeStockConfirm extends HttpServlet {
	private static final long serialVersionUID = -789585356886372348L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// デフォルトの転送先
		String destination = "/WEB-INF/jsp/changeStock/ChangeStockConfirm.jsp";
		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();
		// 在庫変更用ArrayListの作成
		ArrayList<ItemVO> updatedItemList = new ArrayList<ItemVO>();

		//リクエストよりパラメーターのMapを取得
		Map<String,String[]> itemMap = request.getParameterMap();
		// セッションの取得
		HttpSession session = request.getSession(false);

		// モデルクラスをインスタンス化
		ChangeStockLogic changeStockLogic = new ChangeStockLogic();
		try {
			//ロジックの呼び出し
			updatedItemList = changeStockLogic.checkChangeRequest(itemMap);
			if(updatedItemList.isEmpty()){
				error.add("変更対象がありません");
			}
		} catch (NamingException|SQLException|IllegalArgumentException  e) {
			error.add(e.getMessage()+":在庫情報の取得で不具合が発生しています");
		}
		//エラーがあったら追加する
		if(changeStockLogic.hasErrors()) {
			error.addAll(changeStockLogic.getErrors());
		}
		//何らかのエラーがあったらエラーメッセージを指定して差し戻し
		if(!error.isEmpty()){
			//異常系
			//完成したエラーメッセージ用ArrayListをrequestに格納
			request.setAttribute("errormessage",error);
			destination = "/ChangeStock";
		}else{
			//正常系
			//完成した新在庫用ArrayListをセッションに格納
			session.setAttribute("changeStock", updatedItemList);
		}

		// RequestDispatcherオブジェクトを取得して転送
		request.getRequestDispatcher(destination).forward(request, response);

	}

}
