package controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.AddItemLogic;
import vo.ItemVO;
@WebServlet(urlPatterns = "/AddItemComplete")
public class AddItemComplete extends HttpServlet {
	private static final long serialVersionUID = 2199827702924298591L;

	protected void doPost(HttpServletRequest request , HttpServletResponse response)
			throws ServletException,IOException {

		//デフォルトの転送先
		String destination = "/WEB-INF/jsp/addItem/AddItemComplete.jsp";

		//エラーメッセージ処理クラスのインスタンス化
		ArrayList<String> error = new ArrayList<String>();
		//セッションの取得
		HttpSession session = request.getSession(false);

		//作業用Beanの生成
		ItemVO newItem  = null;
		//ロジッククラスのインスタンス生成
		AddItemLogic logic = new AddItemLogic();
		//結果保持用変数
		int result=0;

		//セッションから新規商品のVOを取得
		newItem = (ItemVO)session.getAttribute("newItem");

		if(newItem==null){
			error.add("セッションから新規商品が取得できませんでした。");
		}else{
			try{
				result = logic.insertItem(newItem);//ロジック呼び出し
				//結果が0以下ならエラーメッセージを取得
				if(result<=0){
					error.addAll(logic.getErrors());
				}
			}catch(SQLException|NamingException|IllegalArgumentException e){
				error.add("商品の追加に失敗しました。(根本原因）："+e.getMessage());
			}
			session.removeAttribute("newItem");
		}
		if(!error.isEmpty()){
			//転送先を追加画面に変更
			destination="/AddItem";
			//完成したエラーメッセージ用ArrayListをrequestに格納
			request.setAttribute("errormessage",error);
		}

		request.getRequestDispatcher(destination).forward(request, response);

	}

}
