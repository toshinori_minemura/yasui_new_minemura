package Logger;//java の演習turn10から参照

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

public class CustomLog {
    private static CustomLog _log;
    private CustomLog() {};
    public static CustomLog getInstance() {
        if(_log == null) {
            CustomLog _log1 = new CustomLog();
            _log = _log1;
        }
        return _log;
    }

 public void writeLog(String message) throws IOException {

    String dir = System.getProperty("java.io.tmpdir");
    System.out.println(dir);
    if(!dir.endsWith(File.separator)) {
        StringBuilder dir1 = new StringBuilder(dir);
        dir1.append("/");
        dir = dir1.toString();
    }

    System.out.println(dir);
    StringBuilder dir1 = new StringBuilder(dir);
    dir1.append("Yasui_New_log");
    dir = dir1.toString();

    LocalDateTime dateTime = LocalDateTime.now();

    System.out.println(dir);
    Path path =  Paths.get(dir);

    try(BufferedWriter writer = Files.newBufferedWriter(path,
            StandardCharsets.UTF_8,
            StandardOpenOption.CREATE, StandardOpenOption.APPEND)){
        writer.append(dateTime.toString()+","+message);
        writer.newLine();
    }



    }

}