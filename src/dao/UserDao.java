package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import vo.UserVO;

/**
 * @author 吉原幸伸
 *
 */
public class UserDao extends BaseDao{

	/**
	 * nameでユーザーを1件検索　該当しなければnullをreturn
	 * @param name ユーザー名
	 * @return UserVO ユーザー情報の格納されたBean
	 * @throws SQLException
	 */
	public UserVO getUserByName(Connection con,String name) throws SQLException{
		UserVO user = null;
		String sql = "SELECT user_id,user_name,password,display_name,role FROM users " +
				"where users.is_delete = false and users.user_name = ?";
		try(PreparedStatement pstmt = con.prepareStatement(sql);){
			pstmt.setString(1,name);
			try(ResultSet rs = pstmt.executeQuery();){
				if(rs.next()){
					user = new UserVO();
					user.setId(rs.getString("user_id"));
					user.setName(rs.getString("user_name"));
					user.setPassword(rs.getString("password"));
					user.setDisplayName(rs.getString("display_name"));
					user.setRole(rs.getString("role"));
				}
			}
		}
		return user;
	}
	/**
	 * nameとpasswordでユーザーを1件検索　該当しなければnullをreturn
	 * @param name 入力されたユーザー名
	 * @param password 入力されたパスワード
	 * @return UserVO ユーザー情報の格納されたBean
	 * @throws SQLException
	 */
	public UserVO getUserByName(Connection con,String name,String password) throws SQLException{
		UserVO user = null;
		String sql = "SELECT user_id,user_name,password,display_name,role FROM users " +
				"where users.is_delete = false and users.user_name = ? and users.password = ?";
		try(PreparedStatement pstmt=con.prepareStatement(sql);){
			pstmt.setString(1,name);
			pstmt.setString(2, password);
			try(ResultSet rs = pstmt.executeQuery();){
				System.out.println("(UserDao)name="+name+"/password="+password);
				if(rs.next()){
					System.out.println("(UserDao)found!");
					user = new UserVO();
					user.setId(rs.getString("user_id"));
					user.setName(rs.getString("user_name"));
					user.setPassword(rs.getString("password"));
					user.setDisplayName(rs.getString("display_name"));
					user.setRole(rs.getString("role"));
				}
			}
		}
		return user;
	}
}
