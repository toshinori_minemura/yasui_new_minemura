package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import vo.ContentsVO;


public class ContentsDao extends BaseDao {
	/**
	 * 各ページのTDK情報のマップを取得
	 * @param con
	 * @return HashMap<String,ContentsVO>
	 * @throws SQLException
	 */
	public HashMap<String,ContentsVO> getAllContents(Connection con) throws SQLException{
		HashMap<String,ContentsVO> contents = new HashMap<String,ContentsVO>();
		String sql = "SELECT c.page_id,c.title,c.keyword,c.description,c.role,c.skip" +
				" FROM contents c";
		try(PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();	){
			while(rs.next()){
				ContentsVO content = new ContentsVO();
				content.setPageId(rs.getString("page_id"));
				content.setTitle(rs.getString("title"));
				content.setKeyword(rs.getString("keyword"));
				content.setDescription(rs.getString("description"));
				content.setRole(rs.getString("role"));
				content.setSkip(rs.getInt("skip"));
				contents.put(content.getPageId(), content);
			}
			if(rs!=null)rs.close();
			if(pstmt!=null)pstmt.close();
		}
		return contents;
	}

}
