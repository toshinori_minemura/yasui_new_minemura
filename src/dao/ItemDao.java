package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

import misc.CheckUtil;
import vo.ItemVO;

public class ItemDao extends BaseDao{
	/**
	 * 商品情報全件検索在庫付き
	 * @param con
	 * @return ArrayList<ItemVO>
	 * @throws SQLException
	 */
	public ArrayList<ItemVO> getAllItemsWithStock(Connection con) throws SQLException{

		if(con ==null || con.isClosed()){
			throw new SQLException("接続がNullまたはクローズされています。");
		}
		ArrayList<ItemVO> items = new ArrayList<ItemVO>();
		final String sql = "SELECT i.item_id,i.item_name,i.image_url,i.item_size,i.price,"
				+ "s.quantity FROM items i INNER JOIN stocks s  ON i.item_id = s.item_id  "
				+ " where i.is_delete = false and s.is_delete = false order by i.item_id asc";
		try(PreparedStatement pstmt = con.prepareStatement(sql);
				ResultSet rs = pstmt.executeQuery();){
			while(rs.next()){
				ItemVO item = new ItemVO();
				item.setItemId(rs.getString("item_id"));
				item.setItemName(rs.getString("item_name"));
				item.setImageUrl(rs.getString("image_url"));
				item.setItemSize(rs.getString("item_size"));
				item.setPrice(rs.getInt("price"));
				item.setQuantity(rs.getInt("quantity"));
				items.add(item);
			}
		}
		return items;
	}

	/**
	 * 商品情報１件挿入
	 * @param itemVO 挿入したい商品VO
	 * @return 戻り値は成功したら1 失敗時0
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public int insertItemVO(Connection con,ItemVO itemVO) throws SQLException,IllegalArgumentException{

		int insertItemResult=0;
		int insertStockResult=0;
		if(con ==null || con.isClosed()){
			throw new SQLException("接続がNullまたはクローズされています。");
		}
		if(itemVO==null){
			System.err.println("itemDao#insertItemVO(ItemVO):引数がNull");
			throw new IllegalArgumentException("商品情報が受け取ることができませんでした");//その他の失敗でリターン
		}
		String itemId=itemVO.getItemId();
		String itemName=itemVO.getItemName();
		String url=itemVO.getImageUrl();
		String size=itemVO.getItemSize();
		int price=itemVO.getPrice();
		int quantity=itemVO.getQuantity();

		//重複チェック
		ItemVO temp1 = this.searchItemId(con, itemId);
		ItemVO temp2 = this.searchItemName(con, itemName);

		if(temp1!=null||temp2!=null) {
			throw new IllegalArgumentException("商品名または商品IDが重複しています");
		}

		//商品テーブル更新
		//更新用SQL
		final String sql3 = "INSERT INTO items (item_id,item_name,image_url,item_size,price,is_delete) VALUES (?,?,?,?,?,false)";
		final String sql4 = "INSERT INTO stocks (item_id,quantity,is_delete) VALUES (?,?,false)";
		//トランザクション開始
		try(PreparedStatement pstmt3=con.prepareStatement(sql3);
				PreparedStatement pstmt4=con.prepareStatement(sql4);){
			if(quantity < 0 ){
				throw new SQLException("在庫数に負の値を設定することはできません");//負の在庫設定はできない
			}else{
				//商品テーブル更新
				//自動コミットをoff
				con.setAutoCommit(false);
				pstmt3.setString(1,itemId);
				pstmt3.setString(2,itemName);
				pstmt3.setString(3,url);
				pstmt3.setString(4,size);
				pstmt3.setInt(5,price);

				insertItemResult = pstmt3.executeUpdate();
				if(pstmt3!=null)pstmt3.close();
				pstmt4.setString(1,itemId);
				pstmt4.setInt(2,quantity);
				insertStockResult=pstmt4.executeUpdate();
				if(pstmt4!=null)pstmt4.close();
				if(insertItemResult==0||insertStockResult==0||insertItemResult!=insertStockResult){
					con.rollback();
					throw new SQLException("商品情報と在庫情報の登録の整合性が取れません");
				}
				con.commit();
			}
		}catch(SQLException e){
			con.rollback();
			throw new SQLException(e);
		}
		return insertItemResult;
	}

	/**
	 * 在庫数量変更画面で在庫数量を変更する際に使用する
	 * @param con
	 * @param list 在庫変更する商品VOのリスト
	 * @return 変更した行数
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	//在庫のアップデート　成功すると1、失敗すると-1
	public int updateStock(Connection con,ArrayList<ItemVO> list)throws SQLException,IllegalArgumentException{
		if(con ==null || con.isClosed()){
			throw new SQLException("接続がNullまたはクローズされています。");
		}
		if(list ==null || list.isEmpty()){
			System.err.println("(ItemDao)変更する在庫リストがNullまたは空です。");
			throw new IllegalArgumentException("変更する在庫リストがNullまたは空です。");
		}
		//Listを商品IDがでソート(SQL的には必須ではないが、SQLのスキャン効率的には順番がめちゃくちゃよりは良い）
		Collections.sort(list, new java.util.Comparator<ItemVO>() {
			@Override
            public int compare(ItemVO o1,ItemVO o2) {
				return o1.getItemId().compareTo(o2.getItemId());

			}
		});

		//行ロックをかけるSQL(select ～ for Update）をStringBufferで作成
		String locksql1 = "select item_id,quantity,is_delete from stocks where item_id in (";
		String locksql2 = ") for update";
		StringBuffer locksqlBuilder = new StringBuffer().append(locksql1);
		for(int i=1;i<=list.size();i++){
			locksqlBuilder.append(" ?");
			if(i<list.size()){
				locksqlBuilder.append(", ");
			}
		}
		locksqlBuilder.append(locksql2);
		String locksql = locksqlBuilder.toString();

		//在庫更新用のSQL
		final String updateSql="update stocks set quantity= ? where item_id = ?";

		int result=-1;
		try(PreparedStatement pstmt=con.prepareStatement(locksql)){
			//トランザクション開始
			con.setAutoCommit(false);
			//ロックをかける
			int placeHolder=1;
			for(ItemVO ib:list){
				pstmt.setString(placeHolder,ib.getItemId());
				++placeHolder;
			}
			//ロック実行
			try(ResultSet rs = pstmt.executeQuery()){
				if(!rs.next()){
					throw new SQLException("ロックができませんでした");
				}
				if(rs!=null)rs.close();
			}
			if(pstmt!=null)pstmt.close();
			//更新処理
			try(PreparedStatement pstmt2=con.prepareStatement(updateSql)){
				//更新の実行
				for(ItemVO itemVO:list){
					int newStock = itemVO.getQuantity()-itemVO.getOrder();
					if(newStock<0)throw new SQLException("在庫変更の対象数字が不正です="+itemVO.getItemName()+" 新在庫数="+newStock);
					pstmt2.setInt(1,newStock);
					pstmt2.setString(2,itemVO.getItemId());
					result += pstmt2.executeUpdate();
				}
				if(pstmt!=null)pstmt2.close();
			}
			con.commit();
		}catch(SQLException e){
			e.printStackTrace();
			con.rollback();
			throw new SQLException(e);
		}
		return result;
	}




	/**
	 * 在庫のアップデートを行い、注文情報まで挿入を行う
	 * @param con
	 * @param order 注文のList
	 * @param userName 注文者のユーザー名
	 * @return 成功すると1以上更新した行数
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public int processOrder(Connection con, String userName,ArrayList<ItemVO> order)throws SQLException,IllegalArgumentException{
		int result=0;
		int updateResult=0;
		String userId=null;

		//Connectionチェック
		if(con ==null || con.isClosed()){
			throw new SQLException("接続がNullまたはクローズされています。");
		}
		if(order==null||order.isEmpty()){
			throw new IllegalArgumentException("注文情報がNullまたは空です。");
		}

		//注文テーブルに格納するuidを取得（ユーザー名はセッションから取得できているので）
		UserDao ud = new UserDao();
		userId=ud.getUserByName(con,userName).getId();

		//行ロックをかけるSQL(select ～ for Update）をStringBufferで作成
		String locksql1 = "select item_id,quantity,is_delete from stocks where item_id in (";
		String locksql2 = ") for update";
		StringBuffer locksqlBuilder = new StringBuffer().append(locksql1);
		for(int i=1;i<=order.size();i++){
			locksqlBuilder.append(" ?");
			if(i<order.size()){
				locksqlBuilder.append(", ");
			}
		}
		locksqlBuilder.append(locksql2);
		String locksql = locksqlBuilder.toString();
		System.out.println("作成できたロック用SQL"+locksql);

		//在庫更新のSQL
		String updateSql = "update stocks set quantity=? where item_id=?";

		//繰り返し処理を行う前に、PreparedStatementにSQLを渡してキャッシュ効果を高める
		try(
				PreparedStatement pstmt = con.prepareStatement(locksql);//ロックをかけるSelect文
				PreparedStatement pstmtUpdate = con.prepareStatement(updateSql);
			){

			//自動コミットを切る
			con.setAutoCommit(false);

			//ロックをかける
			int i=1;
			for(ItemVO ib:order){
				pstmt.setString(i,ib.getItemId());
				++i;
			}
			try(ResultSet rs = pstmt.executeQuery()){
				//ロックできたかどうかチェック
				if(!rs.next()){
					throw new SQLException("更新対象のロックに失敗しました");
				}
			}

			//注文のListから商品情報を1つずつ取り出して処理を行う
			for(ItemVO itemVO: order){
				//新しい在庫を計算
				int newStock = itemVO.getQuantity()-itemVO.getOrder();
				//ここまで来る場合通常は在庫は0以上のはずだが、競合対策でチェックする
				int tempResult1 = 0;
				if(newStock>=0){
					pstmtUpdate.setLong(1, newStock);
					pstmtUpdate.setString(2, itemVO.getItemId());
					tempResult1 = pstmtUpdate.executeUpdate();
					if(tempResult1 ==0){
						throw new SQLException("在庫情報の更新に失敗しました");
					}
					//更新できた行数の更新
					result+=tempResult1;
				}
				//注文情報の挿入
				updateResult+=this.insertOrder(con, itemVO.getItemId(), itemVO.getQuantity(), userId);
			}

			System.out.println("updateResult="+updateResult+"/ result="+result);
			//エラーなくループを抜けたが、更新や挿入ができていなかったら
			//Exceptionはないがどちらかの処理が0で終わっていたら
			if(updateResult==0||result==0){
				throw new SQLException("更新ができませんでした");//外側のcatchでキャッチ
			}else if(result!=updateResult) {
				throw new SQLException("在庫の更新件数と、注文の挿入件数が一致しませんでした。");
			}
			//問題なくループを抜けたのでコミットする
			con.commit();
		}catch(SQLException e){
			con.rollback();
			throw new SQLException(e);
		}
		return result;
	}


	/**
	 * 指定された商品VOリストの商品を一括削除する
	 * @param con
	 * @param targetItems 削除対象商品のArrayList
	 * @return int 削除した行数
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public int removeItemList(Connection con, ArrayList<ItemVO> targetItems) throws SQLException,IllegalArgumentException{
		final String sql1 = "UPDATE items SET items.is_delete = true where item_id = ?";
		final String sql2 = "UPDATE stocks SET stocks.is_delete = true where item_id = ?";
		String locksql = null;
		if(targetItems ==null || targetItems.isEmpty()){
			throw new SQLException("削除対象のArrayListがNullまたは空です。");
		}
		if(con ==null || con.isClosed()){
			throw new IllegalArgumentException("接続がNullまたはクローズされています。");
		}

		int result=0;
		//ここに来ている時点でtargetItemsは空ではない
		if(targetItems.size()==1){
			locksql = "select item_id,item_name,image_url,item_size,price,is_delete from items where item_id = ? for update";
		}else if (targetItems.size() > 1){
			//行ロックをかけるSQL(select ～ for Update）をStringBufferで作成
			String locksql1 = "select item_id,item_name,image_url,item_size,price,is_delete from items where item_id in (";
			String locksql2 = ") for update";
			StringBuffer locksqlBuilder = new StringBuffer().append(locksql1);
			for(int i=1;i<=targetItems.size();i++){
				locksqlBuilder.append(" ?");
				if(i<targetItems.size()){
					locksqlBuilder.append(", ");
				}
			}
			locksqlBuilder.append(locksql2);
			locksql = locksqlBuilder.toString();
		}
		System.out.println("作成できたロック用SQL"+locksql);

		try(	PreparedStatement pstmt0=con.prepareStatement(locksql);
				PreparedStatement pstmt1=con.prepareStatement(sql1);
				PreparedStatement pstmt2 = con.prepareStatement(sql2);){
			con.setAutoCommit(false);

			int i=1;
			for(ItemVO targetItem:targetItems){
				pstmt0.setString(i, targetItem.getItemId());
				i++;
			}
			//行ロックをかける
			try(ResultSet rs = pstmt0.executeQuery()){
				if(rs.next()){//ロック成功なら
					for(ItemVO targetItem:targetItems){
						pstmt1.setString(1,targetItem.getItemId());
						int itemResult = pstmt1.executeUpdate();
						pstmt2.setString(1,targetItem.getItemId());
						int stockResult = pstmt2.executeUpdate();
						//両方アップデート成功したら
						if(itemResult==1&&stockResult==1){
							result+=itemResult;//resultを1増やす
						}else{
							throw new SQLException("削除が途中で失敗しました");
						}
					}
				}
				if(rs!=null)rs.close();
			}
			con.commit();
		}catch(SQLException e){
			con.rollback();
			throw new SQLException(e);
		}
		return result;
	}


	/**
	 * 追加する商品用に商品IDを取得する
	 * 本来はDBの機構によって取得をするべきだが、この時点ではmax関数によって取得する
	 * @return 文字列化した商品ID（5桁揃え）
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	synchronized public String getNextItemId(Connection con)throws SQLException,IllegalArgumentException{

		String sql = "select max(item_id) as maxid from items";
		String nextId = null;

		String temp = "99999999"; //ダミー
		int currentMax=-1;//ダミー

		try(PreparedStatement pstmt = con.prepareStatement(sql)){

			try(ResultSet rs = pstmt.executeQuery()){
				if(rs.next()){
					temp = rs.getString("maxid");
				}
			}

		}
		CheckUtil cu = new CheckUtil();
		if(cu.numberTypeCheck(temp, "商品IDの最大値")) {
			currentMax = Integer.parseInt(temp);
			DecimalFormat df = new DecimalFormat("00000000");
			nextId = df.format(currentMax+1);
		}else {
			throw new IllegalArgumentException(cu.getErrors().get(0));
		}

		return nextId;
	}

	/**
	 * 指定された商品IDを検索する
	 * @param con
	 * @param itemId
	 * @return ItemVO
	 * @throws SQLException 同じ商品名が複数あったか、検索処理が失敗した
	 */
	public ItemVO searchItemId(Connection con,String itemId) throws SQLException {
		ItemVO item = null;
		final String sql = "select i.item_id,i.item_name,i.image_url,i.item_size,i.price,s.quantity,"
				+ "i.is_delete from items i inner join stocks s on i.item_id = s.item_id "
				+ "where i.item_id = ?";
		try(PreparedStatement stmt = con.prepareStatement(sql);) {
			stmt.setString(1, itemId);
			try(ResultSet rs = stmt.executeQuery();){
				if(rs.next()){
					item = new ItemVO();
					String item_idDB=rs.getString("item_id");
					String item_nameDB=rs.getString("item_name");
					String image_url=rs.getString("image_url");
					String item_size=rs.getString("item_size");
					int price=rs.getInt("price");
					int quantity=rs.getInt("quantity");
					boolean is_delete = rs.getBoolean("is_delete");
					item.setItemId(item_idDB);
					item.setItemName(item_nameDB);
					item.setImageUrl(image_url);
					item.setItemSize(item_size);
					item.setPrice(price);
					item.setQuantity(quantity);
					item.setDelete(is_delete);
				}
				if(rs.next()) {
					System.err.println("同じ商品IDが複数登録されています");
					throw new SQLException("同じ商品IDが複数登録されています");
				}
			}
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
			throw e;
		}
		return item;
	}
	/**
	 * 指定された商品名を検索する
	 * @param con
	 * @param itemName
	 * @return ItemVO
	 * @throws SQLException 同じ商品名が複数あったか、検索処理が失敗した
	 */
	public ItemVO searchItemName(Connection con,String itemName) throws SQLException {
		ItemVO item = null;
		final String sql = "select i.item_id,i.item_name,i.image_url,i.item_size,i.price,s.quantity,"
				+ "i.is_delete from items i inner join stocks s on i.item_id = s.item_id "
				+ "where i.item_name = ?";
		try(PreparedStatement stmt = con.prepareStatement(sql);) {
			stmt.setString(1, itemName);
			try(ResultSet rs = stmt.executeQuery();){
				if(rs.next()){
					item = new ItemVO();
					String item_idDB=rs.getString("item_id");
					String item_nameDB=rs.getString("item_name");
					String image_url=rs.getString("image_url");
					String item_size=rs.getString("item_size");
					int price=rs.getInt("price");
					int quantity=rs.getInt("quantity");
					boolean is_delete = rs.getBoolean("is_delete");
					item.setItemId(item_idDB);
					item.setItemName(item_nameDB);
					item.setImageUrl(image_url);
					item.setItemSize(item_size);
					item.setPrice(price);
					item.setQuantity(quantity);
					item.setDelete(is_delete);
				}
				if(rs.next()) {
					System.err.println("同じ商品名が複数登録されています");
					throw new SQLException("同じ商品名が複数登録されています");
				}
			}
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
			throw e;
		}
		return item;
	}
	/**
	 * 注文情報を挿入する
	 * @param con
	 * @param itemId
	 * @param quantity
	 * @param userId
	 * @throws SQLException
	 */
	public int insertOrder(Connection con,String itemId,int quantity,String userId) throws SQLException {
		final String sql="INSERT INTO orders ( item_id,user_id,quantity) values (?,?,?)";
		int result1=0;
		try(
				PreparedStatement stmt1 = con.prepareStatement(sql);
				) {
			stmt1.setString(1, itemId);
			stmt1.setString(2, userId);
			stmt1.setInt(3, quantity);
			result1 = stmt1.executeUpdate();

			if(result1 == 1) {
				System.out.println("insertOrder#注文記録成功");
			}
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
			throw e;
		}
		return result1;
	}
}
