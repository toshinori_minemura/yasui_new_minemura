package dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

abstract public class BaseDao {
	// DBに接続
	public Connection getConnection() throws NamingException, SQLException{
		Connection con = null;
		final String localName = "java:comp/env/jdbc/yasui";
		DataSource ds = null;
		Context context = null;
		//コンテキストの生成
		context = new InitialContext();
		//コンテキストを検索
		ds = (DataSource)context.lookup(localName);
		// データベースへ接続
		con = ds.getConnection();
		return con;
	}


	public void closeConnection(Connection con) throws SQLException{
		if(con != null){
			con.close();
		}
	}


}
