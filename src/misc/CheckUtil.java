package misc;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingException;

import dao.ItemDao;
import vo.ItemVO;

public class CheckUtil {

	private ArrayList<String> _errs=null;

	public CheckUtil() {
		this._errs = new ArrayList<String>();
	}

	public boolean hasErrors(){
		return !this._errs.isEmpty();
	}

	public void setError(String msg){
		this._errs.add(msg);
	}

	public ArrayList<String> getErrors(){
		return this._errs;
	}
	/**
	 *
	 * @param itemId 重複チェックしたい商品ID
	 * @return 重複していたらfalse していなかったらtrue
	 * @throws SQLException
	 * @throws NamingException
	 */
	public boolean duplicateIdCheck(String itemId) throws SQLException, NamingException {
		ItemDao dao = new ItemDao();
		try(Connection con = dao.getConnection()){
			ItemVO vo = dao.searchItemId(con, itemId);
			if(vo!=null) {
				return false;
			}
		}
		return true;
	}
	/**
	 * 商品名の重複チェック
	 * @param itemName 重複チェックしたい商品名
	 * @return 重複していたらfalse していなかったらtrue
	 * @throws SQLException
	 * @throws NamingException
	 */
	public boolean duplicateNameCheck(String itemName) throws SQLException, NamingException {
		ItemDao dao = new ItemDao();
		try(Connection con = dao.getConnection()){
			ItemVO vo = dao.searchItemName(con, itemName);
			if(vo!=null) {
				return false;
			}
		}
		return true;
	}
	/**
	 * 文字列長をチェックする
	 * @param value String 長さをチェックしたい文字列
	 * @param max int valueの最大長
	 * @param checkTarget valueを説明できる自然言語
	 * @return 指定文字数以内ならtrue そうでなければfalse
	 */
	public boolean lengthCheck(String value,int max,String checkTarget){
		if(value!=null && !value.isEmpty()){
			if(value.length()>max){
				this.setError(checkTarget + "は" + max + "桁以下で入力してください");
				return false;
			}
		}else{
			this.setError(checkTarget+"の値がnullまたは空です。値を入力してください");
			return false;
		}
		return true;
	}
	/**
	 * 整数変換可能かどうか調べる
	 * @param value String 変換できるかどうか確認したい文字列
	 * @param checkTarget String エラーメッセージに結合するvalueを自然言語で説明したもの
	 * @return boolean 整数値に変換可能ならばtrue 不可ならfalse
	 */
	public boolean numberTypeCheck(String value,String checkTarget){
		if(value!=null && !value.isEmpty()){
			try {
				Integer.parseInt(value);
			} catch (NumberFormatException e) {
				this.setError(checkTarget + "は数値で入力してください");
				return false;
			}
		}
		return true;
	}

	/**
	 * 正規表現チェック
	 * @param value String 正規表現のパターンと一致するかどうか調べる
	 * @param ptn String 正規表現のパターン
	 * @param checkTarget String エラーメッセージに結合するvalueを自然言語で説明したもの
	 * @return パターンと一致していたら true 一致していない場合はfalse
	 */
	public boolean regExCheck(String value,String ptn,String checkTarget){
		if(value!=null && !value.isEmpty()){
			Pattern optn=Pattern.compile(ptn,Pattern.CASE_INSENSITIVE);
			Matcher mch=optn.matcher(value);
			if(!mch.find()){
				this.setError(checkTarget + "を正しい形式で入力してください");
				return false;
			}
		}else{
			this.setError(checkTarget + "が空またはNullです");
			return false;
		}
		return true;
	}

}