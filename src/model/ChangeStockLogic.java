package model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.naming.NamingException;

import dao.ItemDao;
import misc.CheckUtil;
import vo.ItemVO;

public class ChangeStockLogic extends AbstractLogic {

	public ChangeStockLogic() {
		super();
	}
	/**
	 * 在庫の更新を行う
	 * @param stockitems
	 * @return int 在庫の更新を行った行数 (-1:エラー)
	 * @throws SQLException
	 * @throws NamingException
	 * @throws IllegalArgumentException
	 */
	public int updateStock(ArrayList<ItemVO> stockitems)throws SQLException,NamingException,IllegalArgumentException {
		ItemDao dao = new ItemDao();
		int result=-1;
		try(Connection con=dao.getConnection()){
			result = dao.updateStock(con,stockitems);
			dao.closeConnection(con);
		}

		return result;
	}
	/**
	 * 送信されたリクエストパラメーターをチェックしてItemVOのリストを作成する
	 * @param itemMap リクエストパラメーターのマップ
	 * @return ArrayList<ItemVO> 更新があった商品リスト
	 * @throws SQLException
	 * @throws NamingException
	 * @throws IOException
	 */
	public ArrayList<ItemVO> checkChangeRequest(Map<String,String[]> itemMap)
			throws SQLException, NamingException, IOException,IllegalArgumentException {
		//エラーメッセージ用作業変数
		String message=null;

		//在庫数のリミット
		final int STOCK_LIMIT = 99999999;

		//チェック用ユーティリティクラスのインスタンス化
		CheckUtil cu = new CheckUtil();
		// 在庫変更用ArrayListの作成
		ArrayList<ItemVO> updatedItemList = new ArrayList<>();

		//現時点での商品リストの取得
		ListItemLogic listItemLogic = new ListItemLogic();
		ArrayList<ItemVO> items = listItemLogic.getItemList();

		//リクエストパラメーターと商品リストのマージ
		for(Map.Entry<String, String[]> itemEntry:itemMap.entrySet()){
			System.out.println("Key="+itemEntry.getKey()+" :Value="+itemEntry.getValue()[0]);
			for(ItemVO updatedItem:items){
				if(updatedItem.getItemId().equals(itemEntry.getKey())){
					//エラーチェックして整数変換不可の場合はエラーメッセージを構築する
					if(cu.numberTypeCheck(itemEntry.getValue()[0], itemEntry.getKey()+"の新在庫数")){
						int newStock = Integer.parseInt(itemEntry.getValue()[0]);
						if(newStock<0){					//新在庫数が負数だったらはじく
							message = updatedItem.getItemName()+"の新在庫数が不正なため、更新できません。";
							this.addError(message);
							System.err.println(message);
							continue;
						}else if(newStock>STOCK_LIMIT){ //新在庫数が制限値より大きかったらはじく
							message = updatedItem.getItemName()+"の新在庫数が制限を超えているため、更新できません。";
							this.addError(message);
							System.err.println(message);
							continue;
						}
						//新在庫数が在庫数と一致しないときは更新用リストに追加する
						if(newStock!=updatedItem.getQuantity()){
							updatedItem.setQuantity(newStock);
							updatedItemList.add(updatedItem);
						}else
							continue;
					}
				}
			}
		}

		//checkUtilのエラーをエラーリストに追加
		this._errs.addAll(cu.getErrors());

		// 変更要求が0行だったらエラーメッセージを入力
		if (updatedItemList.size() == 0){
			message = "在庫を変更する対象が見つからないため、変更できません。";
			this.addError(message);
		}
		return updatedItemList;
	}

}
