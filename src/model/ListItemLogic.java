package model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import javax.naming.NamingException;

import dao.ItemDao;
import misc.CheckUtil;
import vo.ItemVO;

public class ListItemLogic  extends AbstractLogic {

	public ListItemLogic() {
		super();
	}

	/**
	 * 商品のリストを取得する
	 * @return ArrayList<ItemVO> 商品VOのリスト
	 * @throws SQLException
	 * @throws NamingException
	 * @throws IOException
	 */
	public ArrayList<ItemVO> getItemList()throws SQLException,NamingException, IOException{
		ArrayList<ItemVO> itemList=null;
		//商品一覧を取得して、ArrayList作成
		ItemDao itemDao = new ItemDao();
		try(Connection con=itemDao.getConnection()){
			itemList=itemDao.getAllItemsWithStock(con);
		}
		//itemListをソート
		Collections.sort(itemList, new Comparator<ItemVO>(){
			@Override
			public int compare(ItemVO p1, ItemVO p2) {
				//String同士のソートをするときはcompareTo()を使用する
				return p1.getItemId().compareTo(p2.getItemId());
			}
		});
		return itemList;
	}

	/**
	 * 商品のリストを取得する（先の画面から差し戻しされたときに、注文数を反映する）
	 * @param backwardList 先の画面で取得していた注文数を含む商品リスト
	 * @return ArrayList<ItemVO> 注文数をマージした商品VOのリスト
	 * @throws SQLException
	 * @throws NamingException
	 * @throws IOException
	 */
	public ArrayList<ItemVO> getItemList(ArrayList<ItemVO> backwardList) throws SQLException,NamingException, IOException{

		//商品一覧の取得
		ArrayList<ItemVO> itemList=this.getItemList();

		//backwardListをソート
		Collections.sort(backwardList, new Comparator<ItemVO>(){
			@Override
			public int compare(ItemVO p1, ItemVO p2) {
				//String同士のソートをするときはcompareTo()を使用する
				return p1.getItemId().compareTo(p2.getItemId());
			}
		});

		//itemListとBackwardListをマージ
		for(ItemVO item:itemList){
			for(ItemVO backwardItem:backwardList){
				if(item.getItemId().equals(backwardItem.getItemId())){
					item.setOrder(backwardItem.getOrder());
				}
			}
		}

		return itemList;
	}

	/**
	 * 注文情報のチェックを行い、通ったものを注文リストに入れる
	 * @param orderItemMap Map<String,String[]> request.getParameterMap()から取得したマップ
	 * @param orderedItems 注文のあった商品VOのリスト（参照型なので、returnしなくても送信元で利用できる）
	 * @throws SQLException
	 * @throws NamingException
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	public void confirm(Map<String,String[]> orderItemMap,ArrayList<ItemVO> orderedItems)
			throws SQLException, NamingException, IOException, IllegalArgumentException  {
		//最新の商品一覧（在庫付き）を取得
		ArrayList<ItemVO> items = this.getItemList();
		//チェック用ユーティリティクラスのインスタンス化
		CheckUtil cu = new CheckUtil();

		//リクエストパラメーターと商品リストのマージ
		//ここで論理チェックもできるが、長くなりすぎるので本学習用アプリでは分割する
		for(Map.Entry<String, String[]> item:orderItemMap.entrySet()){//Mapを取り扱う拡張for文
			System.out.println("Key="+item.getKey()+" :Value="+item.getValue()[0]);
			for(ItemVO orderItem:items){
				if(orderItem.getItemId().equals(item.getKey())){
					//エラーチェックして整数変換不可の場合はエラーメッセージを構築する
					if(cu.numberTypeCheck(item.getValue()[0], item.getKey()+"の注文数")){
						orderItem.setOrder(Integer.parseInt(item.getValue()[0]));
					}
					break;
				}
			}
		}

		//checkUtilのエラーをエラーリストに追加
		this._errs.addAll(cu.getErrors());

		//itemsの論理チェック（この段階で数値は必ず入っている
		for(ItemVO orderItem:items){
			//注文が負ならばはじく
			if(orderItem.getOrder()<0){//注文数が負だったら
				this.addError(orderItem.getItemName()+"の注文数が不正なため、発注できません。");
				continue;
			}else if(orderItem.getOrder()==0){//注文数0はスキップする
				continue;
			}else if(orderItem.getQuantity()<orderItem.getOrder()){ //注文が在庫より大きかったらはじく
				this.addError(orderItem.getItemName()+"の注文数が在庫"+orderItem.getQuantity()+"を超えているため、発注できません。");
				continue;
			}else{
				orderedItems.add(orderItem);
				System.err.println("注文数"+orderedItems.size());
			}
		}
		// 注文が0行だったら注文可能フラグをfalse
		if (orderedItems.size() == 0){
			this.addError("有効な注文が確認できないため、発注できません。");
		}


	}
}
