package model;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;

import dao.UserDao;
import vo.UserVO;

public class AuthenticateLoginLogic extends AbstractLogic {
	public AuthenticateLoginLogic(){
		super();
	}
	/**
	 * DAOを呼び出してユーザーをユーザー名とパスワードで検索する
	 * @param username
	 * @param password
	 * @return 発見できたらそのユーザーのUserVO、できなかったらnull
	 * @throws SQLException
	 * @throws NamingException
	 */
	public UserVO authCheck(String username,String password)throws SQLException,NamingException{
		UserDao userDao = new UserDao();
		UserVO userVO = null;

		//ユーザー名とパスワードが空でなかったらログインチェック
		if((username!=null&&!username.isEmpty())&&(password!=null&&!password.isEmpty())){
			try(Connection con=userDao.getConnection()){
				//データベースに接続して、ユーザー情報を取得
				userVO=userDao.getUserByName(con,username,password);
			}
		}else {
			this.addError("ユーザー名またはパスワードが指定されていません");
		}
		return userVO;
	}
	/**
	 * UserDAOを呼び出してユーザー名からUIDを取得する
	 * @param Connection
	 * @return String 次の商品ID
	 * @throws SQLException
	 * @throws NamingException
	 * @throws IllegalArgumentException
	 */
	public String getUidByUserName(String userName)throws SQLException,NamingException, IllegalArgumentException{
		//注文テーブルに格納するuidを取得（5ケタ）（ユーザー名はセッションから取得できているので）
		UserDao userDao = new UserDao();
		String userId=null;

		if(userName==null||userName.isEmpty()){
			throw new IllegalArgumentException("注文したユーザーのユーザー名が取得できません。");
		}
		try(Connection con=userDao.getConnection()){
			//データベースに接続して、ユーザー情報を取得
			UserVO userVO = userDao.getUserByName(con,userName);
			if(userVO==null){
				throw new IllegalArgumentException("注文したユーザーのユーザーIDが検索できません。");
			}
			//データベースと切断
			userDao.closeConnection(con);
		}
		return userId;
	}
}
