package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;

import dao.ItemDao;
import vo.ItemVO;

public class ProcessOrderLogic extends AbstractLogic {

	public ProcessOrderLogic() {
		super();
	}
	/**
	 * 注文処理用のモデル
	 * @param orderItems
	 * @param username
	 * @return int 更新できた行数
	 * @throws SQLException
	 * @throws NamingException
	 */
	public int processOrder(ArrayList<ItemVO> orderItems,String username)throws SQLException,NamingException{
		ItemDao dao = new ItemDao();
		//処理の結果を受け取る変数
		int result=0;
		try(Connection con=dao.getConnection()){
			//注文により在庫更新処理の実行
			if(orderItems!=null){
				result = dao.processOrder(con,username, orderItems);
				if(result<=0) {
					this.addError("在庫の更新失敗 更新できた行数：0 更新したかった行数："+orderItems.size());
				}
			}else{
				this.addError("更新失敗:注文情報がnullです");
			}
			dao.closeConnection(con);
		}
		return result;
	}

}
