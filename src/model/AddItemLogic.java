package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;

import dao.ItemDao;
import misc.CheckUtil;
import vo.ItemVO;

public class AddItemLogic extends AbstractLogic {

	public AddItemLogic() {
		super();
	}
	/**
	 * DAOを呼び出して次の商品IDを取得する
	 * @param Connection
	 * @return String 次の商品ID
	 * @throws SQLException
	 * @throws NamingException
	 */
	public String getNextItemId()throws SQLException,NamingException{
		ItemDao itemDao = new ItemDao();
		String nextId=null;

		try(Connection con=itemDao.getConnection();){
			//データベースに接続して、ユーザー情報を取得
			nextId = itemDao.getNextItemId(con);
			//データベースと切断
			itemDao.closeConnection(con);
		}
		return nextId;
	}

	/**
	 * 新規商品の追加処理
	 * @param newItem 新規商品のItemVO
	 * @return int 追加成功したら1 失敗したら0
	 * @throws SQLException
	 * @throws NamingException
	 */
	public int insertItem(ItemVO newItem) throws SQLException,NamingException{
		ItemDao dao = new ItemDao();
		int result=0;
		if(this.hasErrors()){
			this._errs.clear();
		}
		try(Connection con=dao.getConnection()){
			result = dao.insertItemVO(con,newItem);
			dao.closeConnection(con);
		}
		return result;
	}

	/**
	 * リクエストパラメーターの内容をチェックしてItemVOを作成する
	 * @param params request.getParameterMap()から取得したMap<String,String[]>オブジェクト
	 * @return ItemVO チェック済みのItemVO
	 * @throws SQLException
	 * @throws NamingException
	 */
	public ItemVO itemCheck(Map<String,String[]> params) throws SQLException, NamingException{
		String item_id=null;
		String itemName=null;
		String imgUrl=null;
		String itemSize=null;
		String priceString=null;
		String stockString=null;
		int price=0;
		int stock=0;
		ItemVO newItem=null;

		//ロジックにエラーが溜まっていたら一回クリアする
		if(this.hasErrors()){
			this._errs.clear();
		}

		//チェック用ユーティリティクラスをインスタンス化
		CheckUtil cu = new CheckUtil();
		//まず、nullチェックを行う
		for (HashMap.Entry<String, String[]> entry : params.entrySet()) {
			String key = entry.getKey();
			String[] value = entry.getValue();

			if(key==null){
				//文字列のswitchはJava7以降使用可能になったが、文字列が空の場合はdefaultにいかずにNullPointerException
				//になるため、Nullチェックが必須
				this.addError("不正なデータが送信されています");
				System.err.println("keyが取得できませんでした。");
			}else switch(key) {
			case "item_id":
				if(value.length>1||value.length==0){
					this.addError("item_id:不正なデータ（商品ID）が送信されています");
				}else{
					for(int i=0;i<value.length;i++){
						item_id=value[i];
						System.out.println("item_id="+item_id);
						if(item_id==null||item_id.isEmpty()){
							this.addError("商品IDが入力されていません");
						}
					}
				}
				break;
			case "item_name":
				if(value.length>1||value.length==0){
					this.addError("item_name:不正なデータ（商品名）が送信されています");
					System.err.println("item_nameが不正");
				}else{
					for(int i=0;i<value.length;i++){
						itemName=value[i];
						if(itemName==null||itemName.isEmpty()){
							this.addError("商品名が入力されていません");
						}
					}
				}
				break;
			case "imgurl":
				if(value.length>1||value.length==0){
					this.addError("imgurl:不正なデータ（画像ファイルパス）が送信されています");
				}else{
					for(int i=0;i<value.length;i++){
						imgUrl=value[i];
						if(imgUrl==null||imgUrl.isEmpty()){
							this.addError("商品画像URLが入力されていません");
						}
					}
				}
				break;
			case "item_size":
				if(value.length>1||value.length==0){
					this.addError("item_size:不正なデータ（商品サイズ）が送信されています");
				}else{
					for(int i=0;i<value.length;i++){
						itemSize=value[i];
						System.out.println("item_size="+itemSize);
						if(itemSize==null||itemSize.isEmpty()){
							this.addError("商品サイズが入力されていません");
						}
					}
				}
				break;
			case "price":
				//priceのvalueがnullではなく、値が1つだけ送信されているかチェック
				if(value==null||value.length==0||value.length>1){
					this.addError("price:不正なデータ（価格）が送信されています");
					System.err.println("priceが不正");
				}else{
					priceString=value[0];
					if(priceString==null||priceString.isEmpty()){
						this.addError("商品価格が入力されていません");
					}
				}
				break;
			case "stock":
				//quantityのvalueがnullではなく、値が1つだけ送信されているかチェック
				if(value==null||value.length==0||value.length>1){
					this.addError("quantity:不正なデータ（入庫数量）が送信されています");
				}else{
						stockString=value[0];
						if(stockString==null||stockString.isEmpty()){
							this.addError("商品入庫数が入力されていません");
						}
				}
				break;
			case "submit":
				break;
			default:
				this.addError("不正なデータ（キー）が送信されています:"+key);  // どのcaseにも一致しなかった時に実行される
			}
		}

		//入力漏れエラーが記録されているとNullぽになるので、エラーサイズが0の場合だけ形式チェックを行う
		if(!this.hasErrors()){
			//入力データのチェック
			//価格と在庫が数値型かどうかのチェック
			if(cu.numberTypeCheck(priceString, "価格")&&cu.numberTypeCheck(stockString, "在庫")){
				try{
					price=Integer.parseInt(priceString);
					stock=Integer.parseInt(stockString);
				}catch(NumberFormatException e){
					//ここには到達しない
				}
			}else {
				this.addError("入力された価格または在庫の形式が不正です。数値で入力してください：");
			}

			//商品IDの重複チェック
			if(!cu.duplicateIdCheck(item_id)){
				this.addError("入力された商品ID"+item_id+"が重複しています");
			}

			//商品名の重複チェック
			if(!cu.duplicateNameCheck(itemName)){
				this.addError("入力された商品名"+itemName+"が重複しています");
			}

			//商品名のパターンチェック
			if(!cu.regExCheck(itemName, "\\(.*\\)|（.*）$", "商品名")){
				this.addError("入力された商品名の形式が不正です。例：商品名（色）");
			}

			//URLのパターンチェック
			if(!cu.regExCheck(imgUrl, "(jpeg|jpg|gif|png)$", "画像ファイルパス")){
				this.addError("入力された画像ファイルパスの形式が不正です。例：/images/ファイル名");
			}

			//サイズのパターンチェック
			if(!cu.regExCheck(itemSize, "^[0-9]+[xX][0-9]+[xX][0-9]+$", "サイズ")){
				this.addError("入力された商品のサイズの形式が不正です。例：縦ｘ横ｘ高さ(cm)");
			}

			//入力データの論理チェック
			//価格や在庫数がマイナスだった場合canAddフラグをfalseに設定
			if(price < 0 || price > 99999999){
				this.addError("入力された価格が不正です。");
			}

			if(stock < 0 || stock > 99999999){
				this.addError("入力された在庫数が不正です。");
			}

		}
		//必須チェックも形式チェックでもエラーがなかったら
		if(!this.hasErrors()){
			//正常を確認できた場合だけ、新規商品用のデータをItemVOに収める
			newItem = new ItemVO();
			newItem.setItemId(item_id);
			newItem.setItemName(itemName);
			newItem.setImageUrl(imgUrl);
			newItem.setItemSize(itemSize);
			newItem.setPrice(price);
			newItem.setQuantity(stock);
		}
		return newItem;
	}


}
