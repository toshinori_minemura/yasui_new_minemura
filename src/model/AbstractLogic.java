package model;

import java.util.ArrayList;

public abstract class AbstractLogic {

	public AbstractLogic() {
		this._errs = new ArrayList<String>();
	}
	/**
	 * エラー用ArrayList
	 */
	protected ArrayList<String> _errs=null;
	/**
	 * エラーのArrayListが空かどうか調べる
	 * @return boolean エラーが空じゃなかったらtrue
	 */
	public boolean hasErrors(){
		return !this._errs.isEmpty();
	}
	/**
	 * エラーのArrayListにエラーメッセージを追加する
	 * @param msg 追加するエラーメッセージ
	 */
	public void addError(String msg){
		this._errs.add(msg);
	}
	/**
	 * エラーのArrayListを取得する
	 * @return ArrayList<String> エラーのArrayList
	 */
	public ArrayList<String> getErrors(){
		return this._errs;
	}
}
