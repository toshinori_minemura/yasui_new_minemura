package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.naming.NamingException;

import dao.ContentsDao;
import vo.ContentsVO;

public class ListContentsLogic extends AbstractLogic  {

	public ListContentsLogic() {
		super();
	}
	/**
	 * コンテンツのリストを取得する
	 * @return HashMap<String,ContentsVO> コンテンツのIDとコンテンツVOのマップ
	 * @throws SQLException
	 * @throws NamingException
	 */
	public HashMap<String,ContentsVO> getContentsMap()throws SQLException,NamingException{
		ContentsDao contentsDao = new ContentsDao();
		HashMap<String,ContentsVO> contentsMap = new HashMap<String,ContentsVO>();

		//ユーザー名とパスワードが空でなかったらログインチェック
		try(Connection con=contentsDao.getConnection();){
			//データベースに接続して、コンテンツ情報を取得
			contentsMap=contentsDao.getAllContents(con);
			if(contentsMap.isEmpty()){
				contentsMap = null;
			}
			contentsDao.closeConnection(con);
		}
		return contentsMap;
	}
}
