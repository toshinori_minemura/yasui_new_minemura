package model;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.naming.NamingException;

import dao.ItemDao;
import vo.ItemVO;

public class RemoveItemLogic extends AbstractLogic {

	public RemoveItemLogic() {
		super();
	}
	/**
	 * 引数で与えられたら商品VOリストの商品を削除する
	 * @param targetItems 削除したい商品の商品VOのリスト
	 * @return 削除した行数
	 * @throws SQLException
	 * @throws NamingException
	 * @throws IllegalArgumentException
	 */
	public int removeItem(ArrayList<ItemVO> targetItems)throws SQLException,NamingException,IllegalArgumentException {
		int result=0;
		ItemDao dao = new ItemDao();
		try(Connection con=dao.getConnection();){
			result = dao.removeItemList(con,targetItems);
			dao.closeConnection(con);
		}
		return result;
	}

	/**
	 * リクエストパラメーターと商品リストをマージする
	 * @param itemMap Map<String,String[]> request.getParameterMap()で取得したリクエストパラメーター
	 * @return 削除対象の商品VOのリスト
	 * @throws IOException
	 * @throws NamingException
	 * @throws SQLException
	 */
	public ArrayList<ItemVO> checkRemoveRequest(Map<String,String[]> itemMap)
			throws SQLException, NamingException, IOException{
		//削除商品用ArrayListの作成
		ArrayList<ItemVO> targetItems = new ArrayList<ItemVO>();

		//ロジッククラスのインスタンス作成
		ListItemLogic listItemLogic = new ListItemLogic();
		//商品一覧用ArrayList
		ArrayList<ItemVO> items = listItemLogic.getItemList();

		//リクエストパラメーターと商品リストのマージ
		for(Map.Entry<String, String[]> mappedItem:itemMap.entrySet()){
			System.out.println("Key="+mappedItem.getKey()+" :Value="+mappedItem.getValue()[0]);
			for(ItemVO item:items){
				if(item.getItemId().equals(mappedItem.getKey())){
					targetItems.add(item);
					break;
				}
			}
		}

		return targetItems;
	}

}
