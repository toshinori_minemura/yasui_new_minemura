package secure;

public class HashSample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inputString = "password";
		String hashedString="";
		System.out.println("変換前の文字列"+inputString);
		Digest digest = new Digest(Digest.SHA512);
		hashedString=digest.hex(inputString);
		System.out.println("変換後の文字列は"+hashedString);
	}

}
