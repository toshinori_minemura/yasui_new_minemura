package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * Servlet Filter implementation class EncodingFilter
 */
@WebFilter(
    filterName = "EncodingFilter",
    urlPatterns = { "/index","/ListItem","/PurchaseConfirm","/PurchaseComplete",
    		"/AddItem","/AddItemConfirm","/AddItemComplete",
    		"/ChangeStock","/ChangeStockConfirm","/ChangeStockComplete",
    		"/RemoveItem","/RemoveItemConfirm","/RemoveItemComplete" },
    initParams = {
      @WebInitParam(name = "encoding",value="UTF-8"),
    }
)
public class EncodingFilter implements Filter {
	private String encoding = null;
    public EncodingFilter() {

    }

	public void destroy() {
		this.encoding = null;
	}


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("Encoding filter is invoked.");
		request.setCharacterEncoding(this.encoding);
	    response.setCharacterEncoding(this.encoding);
		response.setContentType("text/html;charset="+this.encoding);
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		this.encoding = fConfig.getInitParameter("encoding");
	}

}
