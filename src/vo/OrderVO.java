package vo;

import java.io.Serializable;
import java.time.LocalDate;

public class OrderVO implements Serializable{

	private String itemId;
	private String userId;
	private String orderId;
	private int quantity;
	private LocalDate orderDate;
	private boolean isCanceled;
	public OrderVO(String itemId, String userId, String orderId, int quantity, LocalDate orderDate,
			boolean isCanceled) {
		super();
		this.itemId = itemId;
		this.userId = userId;
		this.orderId = orderId;
		this.quantity = quantity;
		this.orderDate = orderDate;
		this.isCanceled = isCanceled;
	}
	public OrderVO() {
		super();
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public boolean isCanceled() {
		return isCanceled;
	}
	public void setCanceled(boolean isCanceled) {
		this.isCanceled = isCanceled;
	}


}
