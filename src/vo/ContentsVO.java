package vo;

public class ContentsVO implements java.io.Serializable{

	private static final long serialVersionUID = 8673698103099456835L;
	String title;
	String keyword;
	String description;
	String pageId;
	int skip;
	String role;
	/**
	 * 引数なしコンストラクタ
	 */
	public ContentsVO() {
		super();
	}




	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		if(title!=null&&!title.isEmpty()){
			this.title = title;
		}else{
			System.err.println("(ContentsVO)Titleが空またはnullです。");
		}
	}
	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}
	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(String keyword) {
		if(keyword!=null&&!keyword.isEmpty()){
			this.keyword = keyword;
		}else{
			//System.err.println("(ContentsVO)keywordが空またはnullです。");
		}
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		if(description!=null&&!description.isEmpty()){
			this.description = description;
		}else{
			System.err.println("(ContentsVO)descriptionが空またはnullです。");
		}
	}
	/**
	 * @return the pageId
	 */
	public String getPageId() {
		return pageId;
	}
	/**
	 * @param pageId the pageId to set
	 */
	public void setPageId(String pageId) {
		if(pageId!=null&&!pageId.isEmpty()){
			this.pageId = pageId;
		}else{
			System.err.println("(ContentsVO)pageIdが空またはnullです。");
		}
	}
	/**
	 * @return the skip
	 */
	public int getSkip() {
		return skip;
	}
	/**
	 * @param skip the skip to set
	 */
	public void setSkip(int skip) {
		this.skip = skip;
	}
	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		if(role!=null&&!role.isEmpty()){
			this.role = role;
		}else{
			System.err.println("(ContentsVO)roleが空またはnullです。");
		}
	}
}
