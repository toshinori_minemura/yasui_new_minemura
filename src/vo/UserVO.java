package vo;

public class UserVO implements java.io.Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = -5137407898783534871L;
	String id;
	String name;
	String password;
	String displayName;
	String role;

	/**
	 * 引数なしコンストラクタ
	 */
	public UserVO() {
		super();
	}
	/**
	 * 引数有りコンストラクタ
	 * @param id
	 * @param name
	 * @param password
	 * @param displayName
	 * @param role
	 */
	public UserVO(String id, String name, String password, String displayName, String role) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.displayName = displayName;
		this.role = role;
	}
	/**
	 *
	 * @return id
	 */
	public String getId() {
		return id;
	}
	/**
	 *
	 * @param id
	 */
	public void setId(String id) {
		if(id!=null&&!id.equals("")){
			this.id = id;
		}else{
			System.err.println("(UserVO)idが空またはnullです。");
		}

	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name!=null&&!name.equals("")){
			this.name = name;
		}else{
			System.err.println("(UserVO)nameが空またはnullです。");
		}
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		if(password!=null&&!password.equals("")){
			this.password = password;
		}else{
			System.err.println("(UserVO)passwordが空またはnullです。");
		}
	}

	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		if(role!=null&&!role.equals("")){
			this.role = role;
		}else{
			System.err.println("(UserVO)roleが空またはnullです。");
		}
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		if(displayName!=null&&!displayName.equals("")){
			this.displayName = displayName;
		}else{
			System.err.println("(UserVO)displayNameが空またはnullです。");
		}
	}
}
