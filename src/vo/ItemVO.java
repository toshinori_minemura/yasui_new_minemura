package vo;

import java.io.Serializable;

public class ItemVO implements Serializable {
	private String itemId;
	private String itemName;
	private String imageUrl;
	private String itemSize;
	private int price;
	private boolean isDelete;
	private int quantity;
	private int order;
	//引数ありコンストラクター
	public ItemVO(String itemId, String itemName, String imageUrl, String itemSize, int price, boolean isDelete,
			int quantity, int order) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.imageUrl = imageUrl;
		this.itemSize = itemSize;
		this.price = price;
		this.isDelete = isDelete;
		this.quantity = quantity;
		this.order = order;
	}
	//引数なしコンストラクター
	public ItemVO() {
		super();
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getItemSize() {
		return itemSize;
	}
	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}

}
